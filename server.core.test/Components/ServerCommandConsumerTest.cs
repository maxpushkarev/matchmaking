﻿using NUnit.Framework;
using System;
using System.Globalization;
using System.Reflection;
using System.Threading;
using System.Collections.Generic;
using commons;

namespace server.core.test
{
	[TestFixture]
	internal sealed class ServerCommandConsumerTest
	{
		[Test]
		public void ShouldThrowExceptionWhenHandlerCmdTypeCollision()
		{
			Assert.Throws<ServerExecuteCommandHandlerException>(() => { using (new TestCommandConsumerWithCollision()) { }; });
		}

		[Test]
		public void ShouldThrowExceptionWhenHandlerContextExecutableWrapperCommand()
		{
			Assert.Throws<ServerExecuteCommandHandlerException>(() => { using (new TestCommandConsumerWithExecuteContextCommandHandler()) { }; });
		}

		[Test]
		public void ShouldThrowExceptionWhenHandlerReturnTypeNonVoid()
		{
			Assert.Throws<ServerExecuteCommandHandlerException>(() => { using (new TestCommandConsumerNonVoid()) { }; });
		}

		[Test]
		public void ShouldThrowExceptionWhenHandlerWithoutArgs()
		{
			Assert.Throws<ServerExecuteCommandHandlerException>(() => { using (new TestCommandConsumerEmptyArgs()) { }; });
		}

		[Test]
		public void ShouldThrowExceptionWhenHandlerWithTwoArgs()
		{
			Assert.Throws<ServerExecuteCommandHandlerException>(() => { using (new TestCommandConsumerTwoArgs()) { }; });
		}

		[Test]
		public void ShouldThrowExceptionWhenHandlerAbstractArg()
		{
			Assert.Throws<ServerExecuteCommandHandlerException>(() => { using (new TestCommandConsumerAbstract()) { }; });
		}

		[Test]
		[Repeat(1234)]
		public void ExecutionShouldBeOrdered()
		{
			TestExecutableCommand.CALL_NUMBER = 0;

			TestExecutableCommand c1 = new TestExecutableCommand();
			TestExecutableCommand c2 = new TestExecutableCommand();
			TestExecutableCommand c3 = new TestExecutableCommand();
			TestExecutableCommand c4 = new StopExecutableCommand();

			using (TestCommandConsumer testConsumer = new TestCommandConsumer())
			{
				testConsumer.Start();
				testConsumer.PutCommand(c1);
				testConsumer.PutCommand(c2);
				testConsumer.PutCommand(c3);
				testConsumer.PutCommand(c4);

				testConsumer.Wait();
			}

			Assert.AreEqual(1, c1.callIndex);
			Assert.AreEqual(2, c2.callIndex);
			Assert.AreEqual(3, c3.callIndex);
			Assert.AreEqual(4, c4.callIndex);
		}

		[Test]
		public void HandlerCommandInstanceShouldBeTheSameAsPut()
		{
			SimpleTestCommandConsumer consumer = new SimpleTestCommandConsumer();
			TestExecutableCommand testExecutableCommand = new TestExecutableCommand();
			using (consumer)
			{
				consumer.Start();
				consumer.PutCommand(testExecutableCommand);
				consumer.PutCommand(new StopExecutableCommand());
				consumer.Wait();
			}
			Assert.AreSame(testExecutableCommand, consumer.instance);
		}

		[Test]
		public void ShouldThrowExceptionWhenCommandTypeNotRegistered()
		{
			TestServerConsumerContextCommandExecutor consumer = new TestServerConsumerContextCommandExecutor();
			using (consumer)
			{
				consumer.Start();
				consumer.PutCommand(new TestExecutableCommand());
				consumer.PutCommand(new StopExecutableCommand());
				consumer.Wait();
			}
			Assert.IsAssignableFrom(typeof(ServerExecuteCommandHandlerException), consumer.exception);
		}

		[Test]
		public void ShouldThrowExceptionWhenCommandTypeNotRegisteredToContext()
		{
			TestServerConsumerContextCommandExecutor consumer = new TestServerConsumerContextCommandExecutor();
			using (consumer)
			{
				consumer.Start();
				CommandContextA context = new CommandContextA();
				consumer.RegisterContextExecutableHandler<ContextExecutableCommandA>(
					context, (ContextExecutableCommand cmd) => { }
				);
				consumer.PutCommand(context, new ContextExecutableCommandB());
				consumer.PutCommand(new StopExecutableCommand());
				consumer.Wait();
			}
			Assert.IsAssignableFrom(typeof(ServerExecuteCommandHandlerException), consumer.exception);
		}

		[Test]
		public void HandlerContextCommandShouldBeTheSameInstanceAsPut()
		{
			TestServerConsumerContextCommandExecutor consumer = new TestServerConsumerContextCommandExecutor();
			ContextExecutableCommand actual = null;
			ContextExecutableCommand expected = new ContextExecutableCommandA();

			using (consumer)
			{
				consumer.Start();
				CommandContextA context = new CommandContextA();
				consumer.RegisterContextExecutableHandler<ContextExecutableCommandA>(
					context, (ContextExecutableCommand cmd) => {
						actual = cmd;
					}
				);
				consumer.PutCommand(context, expected);
				consumer.PutCommand(new StopExecutableCommand());
				consumer.Wait();
			}
			Assert.AreSame(expected, actual);
		}

		[Test]
		public void ShouldExecute2SimilarHandlersWithOneContext()
		{
			bool executed1 = false, executed2 = false;

			TestServerConsumerContextCommandExecutor consumer = new TestServerConsumerContextCommandExecutor();
			using (consumer)
			{
				CommandContextA context = new CommandContextA();
				consumer.RegisterContextExecutableHandler<ContextExecutableCommandA>(
					context, cmd => executed1 = true
				);
				consumer.RegisterContextExecutableHandler<ContextExecutableCommandA>(
					context, cmd => executed2 = true
				);
				consumer.Start();
				consumer.PutCommand(context, new ContextExecutableCommandA());
				consumer.PutCommand(new StopExecutableCommand());
				consumer.Wait();
			}

			Assert.IsTrue(executed1);
			Assert.IsTrue(executed2);
		}

		[Test]
		public void ShouldThrowExceptionWhenAbstractCommandTypeRegistered()
		{
			Assert.Throws<ServerExecuteCommandHandlerException>(() => {
				TestServerConsumerContextCommandExecutor consumer = new TestServerConsumerContextCommandExecutor();
				using (consumer)
				{
					consumer.RegisterContextExecutableHandler<ContextExecutableCommand>(
						new CommandContextA(), (ContextExecutableCommand cmd) => { }
					);
				}
			});
		}

		[Test]
		public void OneOfTwoCommandHandlersShouldBeExecutedWithinTheSameContext()
		{
			TestServerConsumerContextCommandExecutor consumer = new TestServerConsumerContextCommandExecutor();
			ContextExecutableCommand commandA = null;
			ContextExecutableCommand commandB = null;

			using (consumer)
			{
				consumer.Start();
				CommandContextA context = new CommandContextA();
				consumer.RegisterContextExecutableHandler<ContextExecutableCommandA>(
					context, (ContextExecutableCommand cmd) => {
						commandA = cmd;
					}
				);
				consumer.RegisterContextExecutableHandler<ContextExecutableCommandB>(
					context, (ContextExecutableCommand cmd) => {
						commandB = cmd;
					}
				);
				consumer.PutCommand(context, new ContextExecutableCommandA());
				consumer.PutCommand(new StopExecutableCommand());
				consumer.Wait();
			}
			Assert.NotNull(commandA);
			Assert.Null(commandB);
		}

		[Test]
		public void ShouldNotExecuteHandlerWhenUnregisterContext()
		{
			TestServerConsumerContextCommandExecutor consumer = new TestServerConsumerContextCommandExecutor();
			ContextExecutableCommand command = null;

			using (consumer)
			{
				consumer.Start();
				CommandContextA context = new CommandContextA();
				consumer.RegisterContextExecutableHandler<ContextExecutableCommandA>(
					context, (ContextExecutableCommand cmd) => {
						command = cmd;
					}
				);
				consumer.UnregisterContext(context);
				consumer.PutCommand(context, new ContextExecutableCommandA());
				consumer.PutCommand(new StopExecutableCommand());
				consumer.Wait();
			}
			Assert.Null(command);
		}

		[Test]
		public void ShotldThrowExceptionWhenUnregisterContext()
		{
			Assert.Throws<ServerExecuteCommandHandlerException>(() => {
				using (TestServerConsumerContextCommandExecutor consumer = new TestServerConsumerContextCommandExecutor())
				{
					consumer.Start();
					consumer.UnregisterContext(new CommandContextA());
				}
			});
		}

		[Test]
		public void CommandHandlerShouldBeExecutedWithinOnlyOneContext()
		{
			TestServerConsumerContextCommandExecutor consumer = new TestServerConsumerContextCommandExecutor();
			ContextExecutableCommand commandA = null;
			ContextExecutableCommand commandB = null;

			using (consumer)
			{
				consumer.Start();

				CommandContextA context1 = new CommandContextA();
				CommandContextA context2 = new CommandContextA();

				consumer.RegisterContextExecutableHandler<ContextExecutableCommandA>(
					context1, (ContextExecutableCommand cmd) => {
						commandA = cmd;
					}
				);
				consumer.RegisterContextExecutableHandler<ContextExecutableCommandA>(
					context2, (ContextExecutableCommand cmd) => {
						commandB = cmd;
					}
				);
				consumer.PutCommand(context1, new ContextExecutableCommandA());
				consumer.PutCommand(new StopExecutableCommand());
				consumer.Wait();
			}
			Assert.NotNull(commandA);
			Assert.Null(commandB);
		}

		[Test]
		public void ContextShouldBeGCCollected()
		{
			TestServerConsumerContextCommandExecutor consumer = new TestServerConsumerContextCommandExecutor();
			WeakReferencesWrapper wrapper = new WeakReferencesWrapper();

			using (consumer)
			{
				new IsolatedCommandContextInitializer(consumer, wrapper);
				GC.Collect();
				GC.WaitForPendingFinalizers();
			}
			Assert.NotZero(wrapper.contextWRs.Count);
			Assert.NotZero(wrapper.handlerWRs.Count);
			wrapper.contextWRs.ForEach(wr => Assert.Null(wr.Target));
			wrapper.handlerWRs.ForEach(wr => Assert.Null(wr.Target));
		}

		[Test]
		public void CanPutContextCommandFromAnotherAppDomain()
		{
			TestServerConsumerContextCommandExecutor consumer = new TestServerConsumerContextCommandExecutor();
			bool executed = false;
			using (consumer)
			{
				consumer.Start();
				CommandContextA context = new CommandContextA();
				consumer.RegisterContextExecutableHandler<ContextExecutableCommandA>(
					context,
					cmd => executed = true
				);
				AppDomainSetup setup = new AppDomainSetup() {
					ApplicationBase = Thread.GetDomain().SetupInformation.ApplicationBase
				};
				AppDomain testAppDomain = AppDomain.CreateDomain("test_context_cmd", null, setup);
				testAppDomain.CreateInstance(
					Assembly.GetAssembly(typeof(SeparateDomainCommandProducer)).FullName, 
					typeof(SeparateDomainCommandProducer).FullName,
					false,
					BindingFlags.Default,
					null,
					new object[] { consumer, context},
					CultureInfo.InvariantCulture,
					null
				);
				consumer.Wait();
			}
			Assert.IsTrue(executed);
		}

		[Test]
		public void ShouldThrowExceptionWhenTransparentProxy()
		{
			TestServerConsumerContextCommandExecutor consumer = new TestServerConsumerContextCommandExecutor();
			using (consumer)
			{
				consumer.Start();
				CommandContextA context = new CommandContextA();
				consumer.RegisterContextExecutableHandler<ContextExecutableCommandA>(
					context,
					c => { }
				);
				AppDomainSetup setup = new AppDomainSetup()
				{
					ApplicationBase = Thread.GetDomain().SetupInformation.ApplicationBase
				};
				AppDomain testAppDomain = AppDomain.CreateDomain("test_context_cmd", null, setup);
				CommandContextA contextProxy = testAppDomain.CreateInstanceAndUnwrap(
					Assembly.GetAssembly(typeof(CommandContextA)).FullName,
					typeof(CommandContextA).FullName
				) as CommandContextA;
				consumer.PutCommand(contextProxy, new ContextExecutableCommandA());
				consumer.PutCommand(new StopExecutableCommand());
				consumer.Wait();
			}
			Assert.IsAssignableFrom<ServerExecuteCommandHandlerException>(consumer.exception);
		}

		private class WeakReferencesWrapper
		{
			public List<WeakReference> contextWRs = new List<WeakReference>();
			public List<WeakReference> handlerWRs = new List<WeakReference>();
		}


		private class IsolatedCommandContextInitializer
		{
			private TestServerConsumerContextCommandExecutor consumer;

			public IsolatedCommandContextInitializer(
				TestServerConsumerContextCommandExecutor consumer, 
				WeakReferencesWrapper wrapper)
			{
				this.consumer = consumer;
				int iterations = Convert.ToInt32(new Random().NextDouble() * 3) + 2;
				for(int i=0; i<=iterations; i++)
				{
					CommandContextA context = new CommandContextA();
					wrapper.contextWRs.Add(new WeakReference(context));
					Init(context, i, wrapper.handlerWRs);
				}
			}

			private void Init(CommandContextA context, int i, List<WeakReference> handlerWrs)
			{
				ContextCommandExecuteHandler<ContextExecutableCommandA> handler = cmd => i++;
				handlerWrs.Add(new WeakReference(handler));
				consumer.RegisterContextExecutableHandler<ContextExecutableCommandA>(
					context,
					handler
				);
			}
		}

		private class SeparateDomainCommandProducer
		{
			public SeparateDomainCommandProducer(
				TestServerConsumerContextCommandExecutor consumer,
				CommandContextA context)
			{
				consumer.PutCommand(context, new ContextExecutableCommandA());
				consumer.PutCommand(new StopExecutableCommand());
			}
		}

		private class BaseTestServerCommandConsumer : ServerCommandConsumer
		{
			public BaseTestServerCommandConsumer() : base(string.Empty)
			{
			}
		}

		private class TestCommandConsumerWithCollision : BaseTestServerCommandConsumer
		{
			[ExecuteCommandHandler]
			private void Test1(TestExecutableCommand cmd)
			{
			}

			[ExecuteCommandHandler]
			private void Test2(TestExecutableCommand cmd)
			{
			}
		}

		private class TestCommandConsumerNonVoid : BaseTestServerCommandConsumer
		{
			[ExecuteCommandHandler]
			private int Test1(TestExecutableCommand cmd)
			{
				return 0;
			}
		}

		private class TestCommandConsumerEmptyArgs : BaseTestServerCommandConsumer
		{
			[ExecuteCommandHandler]
			private void Test1()
			{
			}
		}

		private class TestCommandConsumerTwoArgs : BaseTestServerCommandConsumer
		{
			[ExecuteCommandHandler]
			private void Test1(TestExecutableCommand c1, TestExecutableCommand c2)
			{
			}
		}

		private class TestCommandConsumerAbstract : BaseTestServerCommandConsumer
		{
			[ExecuteCommandHandler]
			private void Test1(BaseExecutableCommand c)
			{
			}
		}

		[Serializable]
		private class TestExecutableCommand : BaseExecutableCommand
		{
			public static int CALL_NUMBER;
			public int callIndex; //not volatile (task creates full fence when start and wait)
		}

		[Serializable]
		private class StopExecutableCommand : TestExecutableCommand
		{
		}

		private class TestCommandConsumer : BaseTestServerCommandConsumer
		{
			[ExecuteCommandHandler]
			private void Stop(StopExecutableCommand cmd)
			{
				RegisterCallIndex(cmd);
				Stop();
			}

			[ExecuteCommandHandler]
			private void Execute(TestExecutableCommand cmd)
			{
				RegisterCallIndex(cmd);
			}

			private void RegisterCallIndex(TestExecutableCommand cmd)
			{
				cmd.callIndex = ++TestExecutableCommand.CALL_NUMBER;
			}
		}

		private class TestCommandConsumerWithExecuteContextCommandHandler : BaseTestServerCommandConsumer
		{
			[ExecuteCommandHandler]
			private void ExecuteContextCommandHandlers(ContextExecutableWrapperCommand cmd)
			{
			}
		}

		private class SimpleTestCommandConsumer : BaseTestServerCommandConsumer
		{
			public TestExecutableCommand instance;

			[ExecuteCommandHandler]
			private void ExecuteTestCommand(TestExecutableCommand cmd)
			{
				instance = cmd;
			}

			[ExecuteCommandHandler]
			private void Stop(StopExecutableCommand cmd)
			{
				Stop();
			}
		}

		private class TestServerConsumerContextCommandExecutor : BaseTestServerCommandConsumer
		{
			public Exception exception;

			[ExecuteCommandHandler]
			private void Stop(StopExecutableCommand cmd)
			{
				Stop();
			}

			protected override void Run()
			{
				try
				{
					base.Run();
				}
				catch (ServerExecuteCommandHandlerException e)
				{
					exception = e;
				}
				catch (TargetInvocationException e)
				{
					exception = e.InnerException;
				}
			}
		}

		private class CommandContextA : CommandContext
		{
		}

		private class CommandContextB : CommandContext
		{
		}

		[Serializable]
		private class ContextExecutableCommandA : ContextExecutableCommand
		{
		}

		private class ContextExecutableCommandB : ContextExecutableCommand
		{
		}
	}
}
