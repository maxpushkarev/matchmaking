﻿using NUnit.Framework;
using System;
using System.Reflection;
using System.Runtime.Remoting;
using System.Security.Permissions;
using System.Threading;

namespace server.core.test
{
	[TestFixture]
	internal sealed class ServerComponentTest
	{
		[Test]
		public void ShouldFinishThreadWhenBlocked()
		{
			TestServerComponentBlocked testServerComponent = new TestServerComponentBlocked();
			using (testServerComponent)
			{
				testServerComponent.Start();
				//has other way to know if thread is blocked
				//signalandwait method is not garanteed to be atomic
				while (!testServerComponent.IsBlocked) {
				}
				testServerComponent.Stop();
				testServerComponent.eventHandle.WaitOne();
			}
			
			Assert.IsTrue(testServerComponent.Aborted);
		}

		[Test]
		public void ShouldCreateMemoryBarrierOnWait()
		{
			TestServerComponentMemoryBarrierWait testServerComponent = new TestServerComponentMemoryBarrierWait();
			testServerComponent.counter++;
			using (testServerComponent)
			{
				testServerComponent.Start();
				testServerComponent.Wait();
			}
			Assert.AreEqual(2, testServerComponent.counter);
		}

		[Test]
		public void ShouldCreateMemoryBarrierOnStop()
		{
			TestServerComponentMemoryBarrierStop testServerComponent = new TestServerComponentMemoryBarrierStop();
			AutoResetEvent evt = new AutoResetEvent(false);
			testServerComponent.counter++;
			testServerComponent.autoResetEvent = evt;
			using (testServerComponent)
			using (evt)
			{
				testServerComponent.Start();
				evt.WaitOne();
				testServerComponent.Stop();
			}
			Assert.AreEqual(2, testServerComponent.counter);
		}

		[Test]
		public void ServerComponentShouldBeEqaulsByRef()
		{
			using (ServerComponent testServerComponent1 = new TestServerComponentMemoryBarrierStop())
			using (ServerComponent testServerComponent2 = testServerComponent1)
			using (ServerComponent testServerComponent3 = new TestServerComponentMemoryBarrierStop())
			{
				Assert.AreEqual(testServerComponent1, testServerComponent2);
				Assert.AreNotEqual(testServerComponent2, testServerComponent3);
				Assert.AreNotEqual(testServerComponent2, null);
			}
		}

		private abstract class BaseTestServerComponent : ServerComponent
		{
			public BaseTestServerComponent() : base(string.Empty)
			{
			}
		}

		private class TestServerComponentMemoryBarrierStop : BaseTestServerComponent
		{
			private bool flag = false;
			public int counter;
			public AutoResetEvent autoResetEvent;

			protected override void Run()
			{
				if(flag)
				{
					return;
				}

				counter++;
				flag = true;
				autoResetEvent.Set();
			}
		}

		private class TestServerComponentMemoryBarrierWait : BaseTestServerComponent
		{
			public int counter;	

			protected override void Run()
			{
				counter++;
				Stop();
			}
		}


		private abstract class BaseServerComponentWithEventHandle : BaseTestServerComponent
		{
			private bool disposed = false;
			public AutoResetEvent eventHandle = new AutoResetEvent(false);
			public bool IsBlocked { get { return ComponentThread.ThreadState == ThreadState.WaitSleepJoin; } }

			public override void Start()
			{
				base.Start();
				ComponentThread.IsBackground = false;
			}

			protected override void Dispose(bool disposing)
			{
				if (disposed)
				{
					return;
				}

				if (disposing)
				{
					eventHandle.Dispose();
				}

				disposed = true;
				base.Dispose(disposing);
			}

		}


		private class TestServerComponentBlocked : BaseServerComponentWithEventHandle
		{
			public bool Aborted { get; private set; }

			protected override void Run()
			{
				try
				{
					Aborted = false;
					eventHandle.WaitOne();
				}
				catch(ThreadAbortException)
				{
					Aborted = true;
					eventHandle.Set();
				}
			}
		}
	}
}