﻿using NUnit.Framework;
using System;
using System.Globalization;
using System.Reflection;
using System.Threading;

namespace server.core.test
{
	[TestFixture]
	internal sealed class MarshalByRefServerObjectTest
	{
		private static volatile bool destroyed;
		private static volatile MarshalByRefServerObject proxy;

		[SetUp]
		public void SetUp()
		{
			destroyed = false;
			proxy = null;
		}

		[Test]
		public void ShouldNotAvoidGCWhenNonreferencedObjectIsMarshalledToCurrentDomain()
		{
			WeakReference weak = null;
			new Action(() => {
			
				AppDomainSetup setup = new AppDomainSetup()
				{
					ApplicationBase = Thread.GetDomain().SetupInformation.ApplicationBase
				};

				AppDomain newDomain = AppDomain.CreateDomain("new", null, setup);
				weak = new WeakReference(newDomain.CreateInstanceAndUnwrap(
					Assembly.GetAssembly(typeof(TestServerObject)).FullName,
					typeof(TestServerObject).FullName,
					false,
					BindingFlags.Default,
					null,
					null,
					CultureInfo.InvariantCulture,
					null
				) as TestServerObject);

			})();
			GC.Collect();
			Assert.False(weak.IsAlive);
		}

		[Test]
		public void ShouldAvoidGCWhenReferencedObjectIsMarshalledToCurrentDomain()
		{
			WeakReference weak = null;
			new Action(() => {

				AppDomainSetup setup = new AppDomainSetup()
				{
					ApplicationBase = Thread.GetDomain().SetupInformation.ApplicationBase
				};

				AppDomain newDomain = AppDomain.CreateDomain("new", null, setup);
				TestServerObject testServerObject = newDomain.CreateInstanceAndUnwrap(
					Assembly.GetAssembly(typeof(TestServerObject)).FullName,
					typeof(TestServerObject).FullName,
					false,
					BindingFlags.Default,
					null,
					null,
					CultureInfo.InvariantCulture,
					null
				) as TestServerObject;
				proxy = testServerObject;
				weak = new WeakReference(testServerObject);

			})();
			GC.Collect();
			Assert.True(weak.IsAlive);
		}

		private class TestServerObject : MarshalByRefServerObject
		{
		}
	}
}
