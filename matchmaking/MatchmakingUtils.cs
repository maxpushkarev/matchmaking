﻿using server.core;
using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("matchmaking.test")]
namespace matchmaking
{
	internal sealed class MatchmakingUtils
	{
		internal static long GetCurrentTime()
		{
			return DateTimeOffset.Now.ToUnixTimeMilliseconds();
		}

		private static long GetFullWaitingTime(int rank1, int rank2, IMatchmakingConfiguration cfg)
		{
			checked
			{
				return cfg.WaitTimeFunc(rank1, rank2);
			}
		}

		private static long GetFullWaitingTime(User user1, User user2, IMatchmakingConfiguration cfg)
		{
			return GetFullWaitingTime(user1.rank, user2.rank, cfg);
		}

		private static long GetFullWaitingTime(MatchmakingUser mmUser1, MatchmakingUser mmUser12, IMatchmakingConfiguration cfg)
		{
			return GetFullWaitingTime(mmUser1.user, mmUser12.user, cfg);
		}

		private static long GetRemainingWaitingTime(MatchmakingUser targetMMUser, long fullWaitingTime)
		{
			long timePassed = GetCurrentTime() - targetMMUser.enterTime;
			return Math.Max(fullWaitingTime - timePassed, 0);
		}

		private static long GetRemainingWaitingTime(MatchmakingUser targetMMUser, MatchmakingUser anotherMMUser, IMatchmakingConfiguration cfg)
		{
			long fullTime = GetFullWaitingTime(targetMMUser, anotherMMUser, cfg);
			return GetRemainingWaitingTime(targetMMUser, fullTime);
		}

		private static long GetRemainingWaitingTime(MatchmakingUser targetMMUser, int minRank, int maxRank, IMatchmakingConfiguration cfg)
		{
			long fullTime = Math.Max(
				GetFullWaitingTime(targetMMUser.user.rank, minRank, cfg),
				GetFullWaitingTime(targetMMUser.user.rank, maxRank, cfg)
			);
			return GetRemainingWaitingTime(targetMMUser, fullTime);
		}

		public static long GetRemainingWaitingTime(MatchmakingUser targetMMUser, int rank, IMatchmakingConfiguration cfg)
		{
			long fullTime = GetFullWaitingTime(targetMMUser.user.rank, rank, cfg);
			return GetRemainingWaitingTime(targetMMUser, fullTime);
		}

		private static long GetRemainingWaitingTime(MatchmakingUser targetMMUser, MatchmakingSet set, IMatchmakingConfiguration cfg)
		{
			return GetRemainingWaitingTime(targetMMUser, set.MinRank, set.MaxRank, cfg);
		}

		internal static long GetMaximumRemainingWaitingTime(MatchmakingSet set, IMatchmakingConfiguration cfg)
		{
			long maximumWaitingTime = 0;
			foreach(MatchmakingSet mmUserSubset in set)
			{
				long remainingTimeForUser = GetRemainingWaitingTime(mmUserSubset.value, set, cfg);
				if(remainingTimeForUser > maximumWaitingTime)
				{
					maximumWaitingTime = remainingTimeForUser;
				}
			}
			return maximumWaitingTime;
		}

		private static long CalculateUserWeight(MatchmakingUser mmUser, MatchmakingSet set, int size, IMatchmakingConfiguration cfg)
		{
			long remainigWaitingTime = GetRemainingWaitingTime(mmUser, set, cfg);
			int usersCount = size;
			return cfg.UserWeightFunc(usersCount, remainigWaitingTime);
		}

		internal static long CalculateUserWeight(MatchmakingUser mmUser, MatchmakingSet set, IMatchmakingConfiguration cfg)
		{
			return CalculateUserWeight(mmUser, set, set.Size, cfg);
		}

		internal static long CalculatePossibleUserWeight(MatchmakingUser mmUser, MatchmakingSet set, IMatchmakingConfiguration cfg)
		{
			return CalculateUserWeight(mmUser, set, set.Size + 1, cfg);
		}
	}
}
