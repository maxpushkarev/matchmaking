﻿using server.core;

namespace matchmaking
{
	internal sealed class MatchmakingUserWrapper
	{
		public readonly MatchmakingSet mmSingleUserSet;
		public readonly CrossDomainCommandContext mmUserContext;
		public MatchmakingSetSizeSegment sizeSegment;
		public MatchmakingSetRankSegment rankSegment;

		public MatchmakingUserWrapper(
			MatchmakingSet mmSingleUserSet,
			CrossDomainCommandContext mmUserContext,
			MatchmakingSetSizeSegment sizeSegment,
			MatchmakingSetRankSegment rankSegment
		)
		{
			this.mmSingleUserSet = mmSingleUserSet;
			this.mmUserContext = mmUserContext;
			this.sizeSegment = sizeSegment;
			this.rankSegment = rankSegment;
		}
	}
}
