﻿namespace matchmaking
{
	internal sealed class MatchmakingSetSizeSegment : MatchmakingSetSegment
	{
		public MatchmakingSetSizeSegment(MatchmakingSet set) : base(set)
		{
		}

		public override void GetSegment(out int min, out int max)
		{
			min = Set.Size;
			max = Set.Size;
		}
	}
}
