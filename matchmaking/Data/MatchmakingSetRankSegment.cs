﻿namespace matchmaking
{
	internal sealed class MatchmakingSetRankSegment : MatchmakingSetSegment
	{
		public MatchmakingSetRankSegment(MatchmakingSet set) : base(set)
		{
		}

		public override void GetSegment(out int min, out int max)
		{
			min = Set.MinRank;
			max = Set.MaxRank;
		}
	}
}
