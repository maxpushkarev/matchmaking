﻿using server.core;

namespace matchmaking
{
	internal sealed class MatchedUser
	{
		public readonly User user;
		public readonly long waitingTime;

		public MatchedUser(MatchmakingUser mmUser, long matchTime)
		{
			this.user = mmUser.user;
			this.waitingTime = matchTime - mmUser.enterTime;
		}

		public override string ToString()
		{
			return string.Format("[{0}, waitingTimeMs: {1}]", user, waitingTime.ToString());
		}
	}
}
