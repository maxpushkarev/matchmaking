﻿using commons;
using System;
using System.Collections;
using System.Collections.Generic;

namespace matchmaking
{
	internal sealed class MatchmakingSet : DSUNode<MatchmakingUser>, IEnumerable<MatchmakingSet>
	{
		private LinkedListNode<MatchmakingSet> selfNodeInParent;
		private LinkedList<MatchmakingSet> childs;

		private bool alive;
		private int minRank;
		private int maxRank;

		public int MinRank
		{
			get { return (FindSet() as MatchmakingSet).minRank; }
			private set { (FindSet() as MatchmakingSet).minRank = value; }
		}

		public int MaxRank
		{
			get { return (FindSet() as MatchmakingSet).maxRank; }
			private set { (FindSet() as MatchmakingSet).maxRank = value; }
		}

		private int size;
		public int Size
		{
			get
			{
				return (FindSet() as MatchmakingSet).size;
			}
			private set
			{
				(FindSet() as MatchmakingSet).size = value;
			}
		}

		private ulong version;
		public ulong Version
		{
			get
			{
				return (FindSet() as MatchmakingSet).version;
			}
			private set
			{
				(FindSet() as MatchmakingSet).version = value;
			}
		}

		public bool Alive
		{
			get
			{
				return (FindSet() as MatchmakingSet).alive;
			}
			set
			{
				(FindSet() as MatchmakingSet).alive = value;
			}
		}

		protected override int Rank => Size;

		public MatchmakingSet(MatchmakingUser mmUser) : base(mmUser)
		{
			childs = new LinkedList<MatchmakingSet>();
			size = 1;
			version = 0;
			alive = true;
			RecalculateRanks();
		}

		public override DSUNode FindSet()
		{
			if (parent == null)
			{
				if(selfNodeInParent != null)
				{
					throw new CorruptedMatchmakingSetException("There is missing reference is set node!");
				}
				return this;
			}

			MatchmakingSet oldParent = parent as MatchmakingSet;
			MatchmakingSet newParent = parent.FindSet() as MatchmakingSet;

			oldParent.childs.Remove(selfNodeInParent);
			selfNodeInParent = newParent.childs.AddLast(this);

			parent = newParent;
			return parent;
		}

		private void RecalculateRanks()
		{
			int minRank = int.MaxValue;
			int maxRank = int.MinValue;

			foreach(MatchmakingSet set in this)
			{
				int userRank = set.value.user.rank;
				if(userRank < minRank)
				{
					minRank = userRank;
				}

				if (userRank > maxRank)
				{
					maxRank = userRank;
				}
			}

			MinRank = minRank;
			MaxRank = maxRank;
		}

		public static MatchmakingSet operator +(MatchmakingSet set1, MatchmakingSet set2)
		{
			MatchmakingSet source;
			MatchmakingSet destination;
			int size = set1.Size + set2.Size;
			Union<MatchmakingSet>(set1, set2, out source, out destination);
			if(!ReferenceEquals(source, destination))
			{
				source.selfNodeInParent = destination.childs.AddLast(source);
				destination.Size = size;
				destination.Version++;
			}
			destination.RecalculateRanks();
			return destination;
		}

		public void ExtractSingle(out MatchmakingSet remainingSet)
		{
			DSUNode root = FindSet();
			remainingSet = root as MatchmakingSet;
			if (ReferenceEquals(root, this) && (Size == 1))
			{
				return;
			}

			if(ReferenceEquals(root, this))
			{
				MatchmakingSet newSetLeader = childs.First.Value;
				childs.Remove(newSetLeader.selfNodeInParent);
				newSetLeader.parent = null;
				newSetLeader.selfNodeInParent = null;

				parent = newSetLeader;
				selfNodeInParent = newSetLeader.childs.AddLast(this);
				newSetLeader.size = size;
				newSetLeader.version = version;
				newSetLeader.alive = alive;
				this.ExtractSingle(out remainingSet);
				return;
			}

			if(childs.Count > 0)
			{
				Queue<MatchmakingSet> reparentingQueue = new Queue<MatchmakingSet>();
				foreach(MatchmakingSet child in childs)
				{
					reparentingQueue.Enqueue(child);
				}

				while(reparentingQueue.Count > 0)
				{
					MatchmakingSet item = reparentingQueue.Dequeue();
					foreach(MatchmakingSet child in item.childs)
					{
						reparentingQueue.Enqueue(child);
					}
					item.FindSet();
				}
			}

			MatchmakingSet mmSetRoot = root as MatchmakingSet;
			mmSetRoot.childs.Remove(selfNodeInParent);
			mmSetRoot.Size--;

			selfNodeInParent = null;
			parent = null;
			childs.Clear();
			Size = 1;

			this.RecalculateRanks();
			mmSetRoot.RecalculateRanks();

			this.Version = ++mmSetRoot.Version;
			this.Alive = mmSetRoot.Alive;
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public IEnumerator<MatchmakingSet> GetEnumerator()
		{
			Queue<MatchmakingSet> enumerationQueue = new Queue<MatchmakingSet>();
			MatchmakingSet root = FindSet() as MatchmakingSet;
			CollectAllElements(root, enumerationQueue);
			
			while(enumerationQueue.Count > 0)
			{	
				MatchmakingSet set = enumerationQueue.Dequeue();
				yield return set;
			}
		}

		private void CollectAllElements(MatchmakingSet root, Queue<MatchmakingSet> enumerationQueue)
		{
			Queue<MatchmakingSet> collectionQueue = new Queue<MatchmakingSet>();
			collectionQueue.Enqueue(root);

			while(collectionQueue.Count > 0)
			{
				MatchmakingSet set = collectionQueue.Dequeue();
				foreach(MatchmakingSet child in set.childs)
				{
					collectionQueue.Enqueue(child);
				}
				enumerationQueue.Enqueue(set);
			}
		}

		public override int GetHashCode()
		{
			return value.GetHashCode();
		}

		public override bool Equals(object obj)
		{
			return ReferenceEquals(this, obj);
		}

		private class CorruptedMatchmakingSetException : Exception
		{
			public CorruptedMatchmakingSetException(string msg) : base(msg)
			{
			}
		}
	}
}
