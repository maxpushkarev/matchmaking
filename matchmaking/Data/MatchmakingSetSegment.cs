﻿using commons;

namespace matchmaking
{
	internal abstract class MatchmakingSetSegment : Segment<int>
	{
		public MatchmakingSet Set { get; set; }

		protected MatchmakingSetSegment(MatchmakingSet set)
		{
			this.Set = set;
		}
	}
}
