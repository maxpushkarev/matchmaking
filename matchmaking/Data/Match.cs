﻿using System;
using System.Linq;

namespace matchmaking
{
	internal sealed class Match
	{
		internal MatchedUser[] matchedUsers;
		private long matchedTime;

		public Match(MatchmakingSet matchedSet, int size)
		{
			matchedUsers = new MatchedUser[size];
			int counter = 0;
			matchedTime = MatchmakingUtils.GetCurrentTime();
			foreach(MatchmakingSet set in matchedSet)
			{
				matchedUsers[counter++] = new MatchedUser(set.value, matchedTime);
			}
		}

		public override string ToString()
		{
			string entranceMsg = string.Format(
				"+++++++++++++++++Matched time: {0}+++++++++++++++++{1}", 
				matchedTime.ToString(),
				Environment.NewLine
			);
			
			string list = string.Join(Environment.NewLine, matchedUsers.Select(mu => mu.ToString()));

			string exitMsg = string.Format(
				"{0}+++++++++++++++++++++++++++++++++++++++++++++++++++{1}",
				Environment.NewLine,
				Environment.NewLine
			);

			return string.Format("{0}{1}{2}", entranceMsg, list, exitMsg);
		}
	}
}
