﻿using server.core;

namespace matchmaking
{
	internal sealed class MatchmakingUser : CommandContext
	{
		public readonly User user;
		public readonly long enterTime;

		public MatchmakingUser(User user)
		{
			this.user = user;
			enterTime = MatchmakingUtils.GetCurrentTime();
		}

		public override bool Equals(object obj)
		{
			return user.Equals(obj);
		}

		public override int GetHashCode()
		{
			return user.GetHashCode();
		}

		public override string ToString()
		{
			return user.ToString();
		}
	}
}
