﻿using commons;
using server.core;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace matchmaking
{
	public abstract class BaseMatchmakingServerComponent : ServerCommandConsumer 
	{
		protected ServerCommandConsumer router;
		protected BaseMatchmakingServerComponent(string name, ServerCommandConsumer router) : base(name)
		{
			this.router = router;
		}

		[ExecuteCommandHandler]
		protected void OnUserEnterMatchmaking(ContextConnectedExecutableCommand<User, CrossDomainCommandContext> cmd)
		{
			ExecuteEnterMatchmaking(cmd);
		}

		protected abstract void ExecuteEnterMatchmaking(ContextConnectedExecutableCommand<User, CrossDomainCommandContext> cmd);
	}

	public abstract class MatchmakingServerComponent<TMMConfig, TCommonConfig> : BaseMatchmakingServerComponent
		where TMMConfig : BaseConfiguration, IMatchmakingConfiguration, new()
		where TCommonConfig : BaseConfiguration, ICommonConfiguration, new()
	{
		private Queue<BaseExecutableCommand> highPriorityMMCommands =
			new Queue<BaseExecutableCommand>();

		private static UpdateUserInMatchmakingCommand UPDATE_CMD_INSTANCE = new UpdateUserInMatchmakingCommand();
		private static RemoveUserFromMatchmakingCommand REMOVE_CMD_INSTANCE = new RemoveUserFromMatchmakingCommand();
		private IMatchmakingConfiguration matchmakingConfiguration;
		private ICommonConfiguration commonConfiguration;
		internal SegmentTree<MatchmakingSetRankSegment, int> rankTree;
		internal SegmentTree<MatchmakingSetSizeSegment, int> sizeTree;

		public MatchmakingServerComponent(string name, ServerCommandConsumer router) : base(name, router)
		{
			matchmakingConfiguration = ConfigurationFactory<TMMConfig>.Get();
			commonConfiguration = ConfigurationFactory<TCommonConfig>.Get();
			sizeTree = new SegmentTree<MatchmakingSetSizeSegment, int>(1, matchmakingConfiguration.UsersInMatchCount);
			rankTree = new SegmentTree<MatchmakingSetRankSegment, int>(commonConfiguration.MinUserRank, commonConfiguration.MaxUserRank);
		}

		protected override void Run()
		{
			while(highPriorityMMCommands.Count > 0)
			{
				Execute(highPriorityMMCommands.Dequeue());
			}
			base.Run();
		}

		protected override void ExecuteEnterMatchmaking(ContextConnectedExecutableCommand<User, CrossDomainCommandContext> cmd)
		{
			User user = cmd.context;
			MatchmakingUserWrapper matchmakingUserWrapper;
			CrossDomainCommandContext mmUserContext = cmd.contextProxy;
			AddNewMMUser(user, mmUserContext, out matchmakingUserWrapper);

			RegisterContextExecutableHandler<ContextDisconnectedExecutableCommand>(mmUserContext,
				c => RemoveUserImmediately(matchmakingUserWrapper.mmSingleUserSet.value)
			);


			RegisterContextExecutableHandler<UpdateUserInMatchmakingCommand>(matchmakingUserWrapper.mmSingleUserSet.value, 
				updateCmd => UpdateUserInMM(matchmakingUserWrapper)
			);

			RegisterContextExecutableHandler<RemoveUserFromMatchmakingCommand>(matchmakingUserWrapper.mmSingleUserSet.value,
				removeCmd => RemoveUser(matchmakingUserWrapper)
			);

			PutCommand(matchmakingUserWrapper.mmSingleUserSet.value, UPDATE_CMD_INSTANCE);
		}

		internal virtual async void UpdateUserInMM(MatchmakingUserWrapper mmUserWrapper)
		{
			rankTree.DFSWithRandomSegments(
				(minRank, maxRank) => {

					int userRank = mmUserWrapper.mmSingleUserSet.value.user.rank;
					int deltaMin = userRank - minRank;
					int deltaMax = userRank - maxRank;

					if (deltaMin * deltaMax <= 0)
					{
						return true;
					}

					int maxUserCount = matchmakingConfiguration.UsersInMatchCount;

					int nearestEdge = minRank;
					if (Math.Abs(deltaMin) > Math.Abs(deltaMax))
					{
						nearestEdge = maxRank;
					}

					long remainingTime = MatchmakingUtils.GetRemainingWaitingTime(mmUserWrapper.mmSingleUserSet.value, nearestEdge, matchmakingConfiguration);
					long segmentWeight = matchmakingConfiguration.UserWeightFunc(maxUserCount, remainingTime);

					return segmentWeight < GetCurrentWeight(mmUserWrapper);

				},
				rankSegment => UpdateMMUser(mmUserWrapper, rankSegment),
				matchmakingConfiguration.MaxRndSetSelectCount
			);


			sizeTree.DFSWithRandomSegments(
				(minSize, maxSize) =>
				{
					int currentSize = GetCurrentSet(mmUserWrapper).Size;
					long remainingTime = 0;
					int possibleUsersCount = Math.Min(matchmakingConfiguration.UsersInMatchCount, maxSize + 1);
					long segmentWeight = matchmakingConfiguration.UserWeightFunc(possibleUsersCount, remainingTime);

					return segmentWeight < GetCurrentWeight(mmUserWrapper) && minSize < matchmakingConfiguration.UsersInMatchCount;
				},
				sizeSegment => UpdateMMUser(mmUserWrapper, sizeSegment),
				matchmakingConfiguration.MaxRndSetSelectCount
			);

			await (Task.Delay(matchmakingConfiguration.ImproveUserWeightInterval));
			PutCommand(mmUserWrapper.mmSingleUserSet.value, UPDATE_CMD_INSTANCE);
		}

		private bool CanStartTheMatch(MatchmakingSet set, out long matchTimeout)
		{
			matchTimeout = MatchmakingUtils.GetMaximumRemainingWaitingTime(set, matchmakingConfiguration);
			return matchTimeout == 0;
		}

		private bool TryUpdateMMUser(MatchmakingUserWrapper mmUserWrapper, MatchmakingSetSegment targetSegment)
		{
			if (!targetSegment.Inserted)
			{
				return false;
			}

			if(!targetSegment.Set.Alive)
			{
				targetSegment.Remove();
				return false;
			}

			if (UserWithinSegment(mmUserWrapper, targetSegment))
			{
				return false;
			}

			if(targetSegment.Set.Size == matchmakingConfiguration.UsersInMatchCount)
			{
				return false;
			}

			long currentWeight = GetCurrentWeight(mmUserWrapper);
			long possibleWeight = MatchmakingUtils.CalculatePossibleUserWeight(mmUserWrapper.mmSingleUserSet.value, targetSegment.Set, matchmakingConfiguration);

			if (possibleWeight >= currentWeight)
			{
				return false;
			}

			ExtractUser(mmUserWrapper);
			int oldSize = targetSegment.Set.Size;
			targetSegment.Set += mmUserWrapper.mmSingleUserSet;
			int newSize = targetSegment.Set.Size;
			targetSegment.Update();


			if(newSize - oldSize != 1)
			{
				throw new InvalidOperationException("Segment should be incremented by one");
			}

			StartMatchIfCan(mmUserWrapper);
			return true;
		}

		private void StartMatchIfCan(MatchmakingUserWrapper mmUserWrapper)
		{
			if (!ValidateSetForMatch(mmUserWrapper.mmSingleUserSet))
			{
				return;
			}

			ScheduleStartMatch(mmUserWrapper.mmSingleUserSet);
		}

		[ExecuteCommandHandler]
		internal void StartMatchCommandHandler(StartMatchCommand cmd)
		{
			MatchmakingSet set = cmd.set;

			if(!ValidateSetForMatch(set))
			{
				return;
			}

			if (cmd.oldSetVersion.Equals(set.Version))
			{
				StartMatch(set);
				return;
			}

			ScheduleStartMatch(set);
		}

		[ExecuteCommandHandler]
		internal void OnMatchCreated(MatchCreatedExecutableCommand cmd)
		{
			ExecuteMatchCreatedCommand(cmd);
		}

		internal virtual void ExecuteMatchCreatedCommand(MatchCreatedExecutableCommand cmd)
		{
			Console.Write(cmd.Match);
		}

		private bool ValidateSetForMatch(MatchmakingSet set)
		{
			if (set.Size < matchmakingConfiguration.UsersInMatchCount)
			{
				return false;
			}

			if (set.Size > matchmakingConfiguration.UsersInMatchCount)
			{
				throw new InvalidOperationException("Limit of users in match exceeded!");
			}

			return true;
		}

		private async void ScheduleStartMatch(MatchmakingSet set)
		{
			long timeToStart;
			if (CanStartTheMatch(set, out timeToStart))
			{
				StartMatch(set);
				return;
			}

			StartMatchCommand startMatchCommand = new StartMatchCommand(set);
			await Task.Delay(TimeSpan.FromMilliseconds(timeToStart));
			PutCommand(startMatchCommand);
		}

		private void StartMatch(MatchmakingSet set)
		{
			SendMatchCreatedCommand(set);
			RemoveMatchFromMM(set);
		}

		internal virtual void SendMatchCreatedCommand(MatchmakingSet set)
		{
			Match match = new Match(set, matchmakingConfiguration.UsersInMatchCount);
			PutCommand(new MatchCreatedExecutableCommand(match));
		}

		private void RemoveMatchFromMM(MatchmakingSet set)
		{
			set.Alive = false;
			foreach (MatchmakingSet single in set)
			{
				MatchmakingSet oldSet;
				single.ExtractSingle(out oldSet);
				RemoveMatchedMMUser(single);
			}
		}

		private void RemoveMatchedMMUser(MatchmakingSet set)
		{
			RemoveUserImmediately(set.value);
		}

		private void RemoveUserImmediately(MatchmakingUser mmUSer)
		{
			highPriorityMMCommands.Enqueue(GenerateContextExecutableCommand(mmUSer, REMOVE_CMD_INSTANCE));
		}

		private void ExtractUser(MatchmakingUserWrapper mmUserWrapper)
		{
			SegmentActionsAfterExtract rankSegmentActions;
			SegmentActionsAfterExtract sizeSegmentActions;

			DefineSegmentActionsAfterExtract(mmUserWrapper, mmUserWrapper.rankSegment, out rankSegmentActions);
			DefineSegmentActionsAfterExtract(mmUserWrapper, mmUserWrapper.sizeSegment, out sizeSegmentActions);

			MatchmakingSet remainigSet;
			mmUserWrapper.mmSingleUserSet.ExtractSingle(out remainigSet);

			UpdateSegmentAfterExtract(mmUserWrapper, mmUserWrapper.rankSegment, rankSegmentActions, remainigSet);
			UpdateSegmentAfterExtract(mmUserWrapper, mmUserWrapper.sizeSegment, sizeSegmentActions, remainigSet);

			mmUserWrapper.rankSegment = null;
			mmUserWrapper.sizeSegment = null;
		}

		private bool UserWithinSegment(MatchmakingUserWrapper mmUserWrapper, MatchmakingSetSegment segment)
		{
			return ReferenceEquals(segment.Set.FindSet(), mmUserWrapper.mmSingleUserSet.FindSet());
		}

		private bool UserRemovedFromMM(MatchmakingUserWrapper mmUserWrapper)
		{
			return !mmUserWrapper.mmSingleUserSet.Alive;
		}

		private void UpdateSegmentAfterExtract(
			MatchmakingUserWrapper mmUserWrapper,
			MatchmakingSetSegment segment,
			SegmentActionsAfterExtract actions,
			MatchmakingSet remainingSet
		)
		{
			if(actions.needRemoveSegmentFromTree)
			{
				segment.Remove();
				return;
			}

			if(actions.needUpdateSegmentWithinTree)
			{
				if(actions.needUpdateRemainingSet)
				{
					segment.Set = remainingSet;
				}
				segment.Update();
			}
		}

		private void DefineSegmentActionsAfterExtract(
			MatchmakingUserWrapper mmUserWrapper, 
			MatchmakingSetSegment segment, 
			out SegmentActionsAfterExtract segmentActions)
		{
			if (!ValidateSegment(mmUserWrapper, segment))
			{
				segmentActions = new SegmentActionsAfterExtract(false, false, false);
				return;
			}

			if((segment.Set.Size == 1) && (ReferenceEquals(segment.Set.FindSet(), mmUserWrapper.mmSingleUserSet)))
			{
				segmentActions = new SegmentActionsAfterExtract(true, false, false);
				return;
			}

			bool segmentDead = !segment.Set.Alive;
			bool userDead = UserRemovedFromMM(mmUserWrapper);

			segmentActions = new SegmentActionsAfterExtract(
				segmentDead, 
				!segmentDead, 
				UserWithinSegment(mmUserWrapper, segment) && !userDead && !segmentDead
			);
		}

		private void UpdateMMUser(MatchmakingUserWrapper mmUserWrapper, MatchmakingSetSizeSegment targetSegment)
		{
			if(TryUpdateMMUser(mmUserWrapper, targetSegment))
			{
				mmUserWrapper.sizeSegment = targetSegment;
			}
		}


		private void UpdateMMUser(MatchmakingUserWrapper mmUserWrapper, MatchmakingSetRankSegment targetSegment)
		{
			if (TryUpdateMMUser(mmUserWrapper, targetSegment))
			{
				mmUserWrapper.rankSegment = targetSegment;
			}
		}

		private void AddNewMMUser(User user, CrossDomainCommandContext context, out MatchmakingUserWrapper matchmakingUserWrapper)
		{
			MatchmakingUser mmUser = new MatchmakingUser(user);
			MatchmakingSet newSet = new MatchmakingSet(mmUser);
			MatchmakingSetSizeSegment sizeSegment = new MatchmakingSetSizeSegment(newSet);
			MatchmakingSetRankSegment rankSegment = new MatchmakingSetRankSegment(newSet);
			sizeTree.Put(sizeSegment);
			rankTree.Put(rankSegment);
			matchmakingUserWrapper = new MatchmakingUserWrapper(newSet, context, sizeSegment, rankSegment);
		}

		private void RemoveUser(MatchmakingUserWrapper mmUserWrapper)
		{
			UnregisterContext(mmUserWrapper.mmUserContext);
			ExtractUser(mmUserWrapper);
			UnregisterContext(mmUserWrapper.mmSingleUserSet.value);
		}

		private MatchmakingSet GetCurrentSet(MatchmakingUserWrapper mmUserWrapper)
		{
			return mmUserWrapper.mmSingleUserSet;
		}

		private bool ValidateSegment(MatchmakingUserWrapper mmUserWrapper, MatchmakingSetSegment segment)
		{
			if(segment == null)
			{
				return false;
			}

			if (!segment.Inserted)
			{
				return false;
			}

			return true;
		}

		private long GetCurrentWeight(MatchmakingUserWrapper mmUserWrapper)
		{
			return MatchmakingUtils.CalculateUserWeight(
				mmUserWrapper.mmSingleUserSet.value,
				GetCurrentSet(mmUserWrapper),
				matchmakingConfiguration
			);
		}

		private struct SegmentActionsAfterExtract
		{
			public readonly bool needRemoveSegmentFromTree;
			public readonly bool needUpdateSegmentWithinTree;
			public readonly bool needUpdateRemainingSet;

			public SegmentActionsAfterExtract(
				bool needRemoveSegmentFromTree,
				bool needUpdateSegmentWithinTree,
				bool needUpdateRemainingSet
			)
			{
				this.needRemoveSegmentFromTree = needRemoveSegmentFromTree;
				this.needUpdateSegmentWithinTree = needUpdateSegmentWithinTree;
				this.needUpdateRemainingSet = needUpdateRemainingSet;
			}
		}
	}

	public sealed class MatchmakingServerComponent : MatchmakingServerComponent<MatchmakingConfiguration, CommonConfiguration>
	{
		public MatchmakingServerComponent(string name, ServerCommandConsumer router) : base(name, router)
		{
		}
	}
}
