﻿using commons;

namespace matchmaking
{
	internal sealed class MatchCreatedExecutableCommand : BaseExecutableCommand
	{
		public Match Match { get; private set; }
		public MatchCreatedExecutableCommand(Match match)
		{
			this.Match = match;
		}
	}
}