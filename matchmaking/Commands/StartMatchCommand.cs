﻿using commons;

namespace matchmaking
{
	internal sealed class StartMatchCommand : BaseExecutableCommand
	{
		public readonly ulong oldSetVersion;
		public readonly MatchmakingSet set;

		public StartMatchCommand(MatchmakingSet set)
		{
			this.set = set;
			this.oldSetVersion = set.Version;
		}
	}
}
