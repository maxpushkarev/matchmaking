﻿using System;

namespace matchmaking
{
	public interface IMatchmakingConfiguration
	{
		Func<int, int, long> WaitTimeFunc { get; }
		Func<int, long, long> UserWeightFunc { get; }
		int UsersInMatchCount { get; }
		int MaxRndSetSelectCount { get; }
		int ImproveUserWeightInterval { get; }
	}
}
