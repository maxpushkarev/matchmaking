﻿using commons;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.Scripting;
using System;
using System.Reflection;
using System.Resources;
using System.Threading.Tasks;

namespace matchmaking
{
	public sealed class MatchmakingConfiguration : BaseConfiguration, IMatchmakingConfiguration
	{
		private ResourceManager resourceManager;
		public Func<int, int, long> WaitTimeFunc { get; private set; }
		public Func<int, long, long> UserWeightFunc { get; private set; }
		public int UsersInMatchCount { get; private set; }
		public int ImproveUserWeightInterval { get; private set; }
		public int MaxRndSetSelectCount { get; private set; }

		public MatchmakingConfiguration()
		{
			resourceManager = new ResourceManager(
				"matchmaking.Matchmaking",
				Assembly.GetAssembly(typeof(MatchmakingConfiguration))
			);
			UsersInMatchCount = Convert.ToInt32(resourceManager.GetString("usersInMatchCount"));
			ImproveUserWeightInterval = Convert.ToInt32(resourceManager.GetString("improveUserWeightInterval"));
			MaxRndSetSelectCount = Convert.ToInt32(resourceManager.GetString("maxRndSetSelectCount"));
			InitExpressions();
		}

		private void InitExpressions()
		{
			ScriptOptions options = ScriptOptions.Default.WithImports("System.Math");
			using (Task waitFunTask = InitWaitTimeExpression(options))
			using (Task weightFunTask = InitWeightExpression(options))
			{
				Task.WaitAll(waitFunTask, weightFunTask);
			}
		}

		private async Task InitWaitTimeExpression(ScriptOptions options)
		{
			string strWaitTimeFunc = resourceManager.GetString("minWaitTimeFunc");
			WaitTimeFunc = await CSharpScript.EvaluateAsync<Func<int, int, long>>(strWaitTimeFunc, options);
		}

		private async Task InitWeightExpression(ScriptOptions options)
		{
			string strWeightTimeFunc = resourceManager.GetString("userWeightFunc");
			UserWeightFunc = await CSharpScript.EvaluateAsync<Func<int, long, long>>(strWeightTimeFunc, options);
		}
	}
}
