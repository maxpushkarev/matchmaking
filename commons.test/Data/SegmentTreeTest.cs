﻿using NUnit.Framework;
using System;

namespace commons.test
{
	[TestFixture]
	public sealed class SegmentTreeTest
	{
		[Test]
		public void ShouldThrowExceptionWhenMinEqualsMax()
		{
			Assert.Throws<ArgumentException>(() => new SegmentTree<TestSegment, int>(2, 2));
		}

		[Test]
		public void ShouldThrowExceptionWhenMinGreaterThanMax()
		{
			Assert.Throws<ArgumentException>(() => new SegmentTree<TestSegment, int>(3, 2));
		}

		[Test]
		public void ShouldThrowExceptionWhenDoubleInitSegment()
		{
			Assert.Throws<InvalidOperationException>(() =>
			{
				TestSegment testSegment = new TestSegment(0, 0);
				testSegment.Init(null, null);
				testSegment.Init(null, null);
			});
		}

		[Test]
		public void ShouldThrowExceptionWhenRemoveNotInitializedSegment()
		{
			Assert.Throws<InvalidOperationException>(() =>
			{
				TestSegment testSegment = new TestSegment(0, 0);
				testSegment.Remove();
			});
		}

		[Test]
		public void ShouldThrowExceptionWhenUpdateNotInitializedSegment()
		{
			Assert.Throws<InvalidOperationException>(() =>
			{
				TestSegment testSegment = new TestSegment(0, 0);
				testSegment.Update();
			});
		}

		[Test]
		public void ShouldThrowExceptionWhenPutOutOfRangeSegment()
		{
			Assert.Throws<ArgumentException>(() => new SegmentTree<TestSegment, int>(2, 17).Put(new TestSegment(1, 1)));
		}

		[Test]
		public void ShouldThrowExceptionWhenLeftLessThanMin()
		{
			Assert.Throws<ArgumentException>(() => new SegmentTree<TestSegment, int>(2, 17).Put(new TestSegment(1, 8)));
		}

		[Test]
		public void ShouldThrowExceptionWhenRightGreaterThanMax()
		{
			Assert.Throws<ArgumentException>(() => new SegmentTree<TestSegment, int>(2, 17).Put(new TestSegment(4, 25)));
		}

		[Test]
		public void ShouldThrowExceptionWhenMinAndMaxDiffMoreThanRootOfTree()
		{
			Assert.Throws<ArgumentException>(() => new SegmentTree<TestSegment, int>(2, 17).Put(new TestSegment(1, 32)));
		}

		[Test]
		public void ShouldThrowExceptionWhenSegmentMinMoreThanSegmentMax()
		{
			Assert.Throws<ArgumentException>(() => new SegmentTree<TestSegment, int>(2, 17).Put(new TestSegment(11, 10)));
		}

		[Test]
		public void ShouldPassPutAndRemove()
		{
			SegmentTree<TestSegment, int> tree = new SegmentTree<TestSegment, int>(2, 17);
			TestSegment testSegment = new TestSegment(4, 6);
			tree.Put(testSegment);
			testSegment.Remove();
			Assert.Pass();
		}

		[Test]
		public void ShouldPassPutUpdateUpdateRemove()
		{
			SegmentTree<TestSegment, int> tree = new SegmentTree<TestSegment, int>(2, 17);
			TestSegment testSegment = new TestSegment(4, 6);
			tree.Put(testSegment);
			testSegment.Update();
			testSegment.Update();
			testSegment.Remove();
			tree.Put(testSegment);
			Assert.Pass();
		}

		[Test]
		public void ShouldPassWhenRightBorderBehindDegreeOfTwo()
		{
			SegmentTree<TestSegment, int> tree = new SegmentTree<TestSegment, int>(2, 18);
			TestSegment testSegment = new TestSegment(4, 33);
			tree.Put(testSegment);
			Assert.Pass();
		}

		[Test]
		public void ShouldPassWhenMinOnBorder()
		{
			SegmentTree<TestSegment, int> tree = new SegmentTree<TestSegment, int>(2, 18);
			TestSegment testSegment = new TestSegment(2, 33);
			tree.Put(testSegment);
			Assert.Pass();
		}

		[Test]
		public void ShouldPassWhenRightBorderAheadDegreeOfTwo()
		{
			Assert.Throws<ArgumentException>(() => {
				SegmentTree<TestSegment, int> tree = new SegmentTree<TestSegment, int>(2, 18);
				TestSegment testSegment = new TestSegment(4, 34);
				tree.Put(testSegment);
			});
		}

		[Test]
		public void ShouldThrowExceptionWhenDoublePut()
		{
			Assert.Throws<InvalidOperationException>(() => {
				SegmentTree<TestSegment, int> tree = new SegmentTree<TestSegment, int>(2, 17);
				TestSegment testSegment = new TestSegment(4, 6);
				tree.Put(testSegment);
				tree.Put(testSegment);
			});
		}

		[Test]
		public void ShouldCollectAllItemsWhenConditionAlwaysTrue()
		{
			SegmentTree<TestSegment, int> tree = new SegmentTree<TestSegment, int>(1, 16);
			tree.Put(new TestSegment(1, 2));
			tree.Put(new TestSegment(2, 7));
			tree.Put(new TestSegment(3, 9));
			tree.Put(new TestSegment(4, 7));
			tree.Put(new TestSegment(8, 8));
			int res = 0;
			tree.DFSWithRandomSegments((min, max) => true, s =>
			{
				int min, max;
				s.GetSegment(out min, out max);
				res |= (1 << min);
			}, int.MaxValue);

			Assert.AreEqual(286, res);
		}


		[Test]
		public void ShouldSkipSubTreeByCondition()
		{
			SegmentTree<TestSegment, int> tree = new SegmentTree<TestSegment, int>(1, 16);
			tree.Put(new TestSegment(1, 6));
			tree.Put(new TestSegment(3, 3));
			tree.Put(new TestSegment(7, 8));
			int res = 0;
			tree.DFSWithRandomSegments((min, max) => min <= 4, s =>
			{
				int min, max;
				s.GetSegment(out min, out max);
				res |= (1 << min);
			}, int.MaxValue);

			Assert.AreEqual(10, res);
		}


		[Test]
		public void ShouldNotCollectAnyItemsWhenConditionAlwaysFalse()
		{
			SegmentTree<TestSegment, int> tree = new SegmentTree<TestSegment, int>(1, 16);
			tree.Put(new TestSegment(1, 2));
			tree.Put(new TestSegment(2, 7));
			tree.Put(new TestSegment(3, 9));
			tree.Put(new TestSegment(4, 7));
			tree.Put(new TestSegment(8, 8));
			int res = 0;
			tree.DFSWithRandomSegments((min, max) => false, s =>
			{
				int min, max;
				s.GetSegment(out min, out max);
				res |= (1 << min);
			}, int.MaxValue);

			Assert.Zero(res);
		}

		[Test]
		public void ShouldSegmentBeGCCollectedWhenRemoved()
		{
			SegmentTree<TestSegment, int> tree = new SegmentTree<TestSegment, int>(1, 16);
			WeakReference wr = null;

			(new Action(() => {
				var segment = new TestSegment(1, 2);
				wr = new WeakReference(segment);
				tree.Put(segment);
				segment.Remove();
			}))();
			GC.Collect();

			Assert.Null(wr.Target);
		}

		internal class TestSegment : Segment<int>
		{
			private int min, max;

			public TestSegment(int min, int max)
			{
				this.min = min;
				this.max = max;
			}

			public override void GetSegment(out int min, out int max)
			{
				min = this.min;
				max = this.max;
			}
		}
	}
}
