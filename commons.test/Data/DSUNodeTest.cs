﻿using NUnit.Framework;

namespace commons.test
{
	[TestFixture]
	internal sealed class DSUNodeTest
	{
		[Test]
		public void ShouldSaveValue()
		{
			int i = 10;
			Assert.AreEqual(i, new DSUNode<int>(i).value);
		}

		[Test]
		public void ShouldSetSelfAsParentWhenCreated()
		{
			DSUNode node = new DSUNode<int>(0);
			Assert.AreSame(node, node.FindSet());
		}

		[Test]
		public void ShouldCorrectSelfUnion()
		{
			DSUNode node = new DSUNode<int>(0);
			DSUNode union = node + node;
			Assert.AreSame(node, union);
		}

		[Test]
		public void ShouldRecursivelyFindParent()
		{
			DSUNode node1 = new DSUNode<int>(0);
			DSUNode node2 = new DSUNode<int>(0);
			DSUNode node3 = new DSUNode<int>(0);
			node2 += node3;
			node1 += node2;

			Assert.AreSame(node1, node3.FindSet());
		}

		[Test]
		public void ShouldNotAttachAlreadyAttachedSet()
		{
			DSUNode node1 = new DSUNode<int>(0);
			DSUNode node2 = new DSUNode<int>(0);
			DSUNode node3 = new DSUNode<int>(0);
			node1 += node2;
			node3 += node2;

			Assert.AreSame(node1, node3.FindSet());
			DSUNode union = node1 + node3;
			Assert.AreSame(node1, union);
		}

		[Test]
		public void ShouldAttachSecondToFirstWhenEqualRanks()
		{
			DSUNode node1 = new DSUNode<int>(0);
			DSUNode node2 = new DSUNode<int>(0);
			DSUNode union = node1 + node2;
			Assert.AreSame(node1, union);
		}

		[Test]
		public void ShouldLessRankedNodeToMoreRankedNode()
		{
			DSUNode node1 = new DSUNode<int>(0);
			DSUNode node2 = new DSUNode<int>(0);
			node1 += node2;
			DSUNode node3 = new DSUNode<int>(0);
			DSUNode union = node3 + node1;
			Assert.AreSame(node1, union);
		}
	}
}
