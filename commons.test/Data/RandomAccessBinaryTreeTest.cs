﻿using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace commons.test
{
	[TestFixture]
	internal sealed class RandomAccessBinaryTreeTest
	{
		[Test]
		public void ShouldReturnEmptyCollectionFromEmptyTree()
		{
			RandomAccessBinaryTree<int> tree = new RandomAccessBinaryTree<int>();
			Assert.IsEmpty(tree.GetRandomElements(123));
		}

		[Test]
		public void ShouldReturnEmptyCollectionFromNonEmptyTree()
		{
			RandomAccessBinaryTree<int> tree = new RandomAccessBinaryTree<int>();
			tree.Add(8);
			Assert.IsEmpty(tree.GetRandomElements(0));
		}

		[Test]
		public void ShouldReturnSingleElementFormSingleNodeTree()
		{
			RandomAccessBinaryTree<int> tree = new RandomAccessBinaryTree<int>();
			tree.Add(8);
			var collection = tree.GetRandomElements(1);
			int count = 0;
			int item = 0;
			foreach(var _item in collection)
			{
				count++;
				item = _item;
			}
			Assert.AreEqual(1, count);
			Assert.AreEqual(8, item);
		}

		[Test]
		[Repeat(77)]
		public void ShouldReturnAllValuesWhenMaxCountGreaterThanSizeOfTree()
		{
			AddElementsAndAssertFetch(8, 16, resCount => 8 == resCount, (fullMask, res) =>
			{
				return fullMask == res;
			});
		}

		[Test]
		[Repeat(77)]
		public void ShouldReturnAllAddedValues()
		{
			AddElementsAndAssertFetch(5, 5, resCount => 5 == resCount, (fullMask, res) =>
			{
				return fullMask == res;
			});
		}

		[Test]
		[Repeat(77)]
		public void ShouldReturnSomeOfAddedElementsWhenRndCountLessThanTreeSize()
		{
			AddElementsAndAssertFetch(8, 1, resCount => 1 == resCount, (fullMask, res) =>
			{
				return (fullMask | res) > 0;
			});
		}

		private void AddElementsAndAssertFetch(int addCount, int rndCount, Func<int, bool> assertCount, Func<int, int, bool> assertRes)
		{
			Random rnd = new Random();
			List<int> values = new List<int>();

			RandomAccessBinaryTree<int> tree = new RandomAccessBinaryTree<int>();
			for (int i = 0; i < addCount; i++)
			{
				values.Add(1 << i);
			}

			while (values.Count > 0)
			{
				int rndIndex = Commons.RndRange(rnd, 0, values.Count);
				int value = values[rndIndex];
				values.RemoveAt(rndIndex);
				tree.Add(value);
			}

			int res = 0;
			var rndElements = tree.GetRandomElements(rndCount);

			int resCount = 0;
			foreach (int val in rndElements)
			{
				res |= val;
				resCount++;
			}

			Assert.IsTrue(assertCount(resCount));
			Assert.IsTrue(assertRes(Convert.ToInt32(Math.Pow(2, addCount)) - 1, res));
		}

		[Test]
		public void ShouldRemoveSingleRootNodeFromTree()
		{
			RandomAccessBinaryTree<int> tree = new RandomAccessBinaryTree<int>();
			var node = tree.Add(123);
			tree.Remove(node);
			var collection = tree.GetRandomElements(5);
			Assert.IsEmpty(collection);
		}

		[Test]
		public void ShouldThrowExceptionWhenTwiceRemoveLeaf()
		{
			Assert.Throws<InvalidOperationException>(() => {
				RandomAccessBinaryTree<int> tree = new RandomAccessBinaryTree<int>();
				var node = tree.Add(123);
				tree.Remove(node);
				tree.Remove(node);
			});
		}

		[Test]
		public void ShouldNodeBeGarbageCollectedWhenRemoved()
		{
			WeakReference wr = null ;
			RandomAccessBinaryTree<int> tree = new RandomAccessBinaryTree<int>();
			(new Action(() => {
				var node = tree.Add(123);
				wr = new WeakReference(node);
				tree.Remove(node);
			}))();
			GC.Collect();
			Assert.Null(wr.Target);
		}

		[Test]
		public void ShouldThrowExceptionWhenTwiceRemoveNonLeaf()
		{
			Assert.Throws<InvalidOperationException>(() => {
				RandomAccessBinaryTree<int> tree = new RandomAccessBinaryTree<int>();
				tree.Add(0);
				tree.Add(0);
				tree.Add(0);
				var node = tree.Add(123);
				tree.Add(0);
				tree.Add(0);
				tree.Add(0);
				tree.Add(0);
				tree.Add(0);
				tree.Add(0);
				tree.Add(0);
				tree.Add(0);
				tree.Add(0);
				tree.Remove(node);
				tree.Remove(node);
			});
		}

		[Test]
		public void ShouldRemoveLeafAfterUp()
		{
			RandomAccessBinaryTree<int> tree = new RandomAccessBinaryTree<int>();
			tree.Add(0);
			var node1 = tree.Add(1);
			tree.Add(2);
			tree.Add(3);
			tree.Add(4);
			tree.Add(5);
			tree.Add(6);
			var node7 = tree.Add(7);

			tree.Remove(node1);
			tree.Remove(node7);

			int count = 0;
			foreach(int item in tree.GetRandomElements(int.MaxValue))
			{
				count++;
			}
			Assert.AreEqual(6, count);
		}

		[Test]
		public void ShouldRemoveRootWithTwoLeafs()
		{
			AddAndRemoveElements(3, 0, 0);
		}

		[Test]
		public void ShouldRemoveLeftLeafFromRoot()
		{
			AddAndRemoveElements(3, 0, 1);
		}

		[Test]
		public void ShouldRemoveRightLeafFromRoot()
		{
			AddAndRemoveElements(3, 0, 2);
		}

		[Test]
		public void ShouldRemoveRightLeafAndRoot()
		{
			AddAndRemoveElements(3, 0, 0, 2);
		}

		[Test]
		public void ShouldRemoveLeftLeafAndRoot()
		{
			AddAndRemoveElements(3, 0, 0, 1);
		}

		[Test]
		public void ShouldRemoveBothLeavesFromRoot()
		{
			AddAndRemoveElements(3, 0, 1, 2);
		}

		[Test]
		public void ShouldRemoveBothLeavesAndRoot()
		{
			AddAndRemoveElements(3, 0, 0, 1, 2);
		}

		[Test]
		public void ShouldRemoveRootWithOneLeaf()
		{
			AddAndRemoveElements(2, 0, 0);
		}

		[Test]
		public void ShouldRemoveOneLeafFromRoot()
		{
			AddAndRemoveElements(2, 0, 1);
		}

		[Test]
		public void ShouldRemoveAllWhenOneLeafFromRoot()
		{
			AddAndRemoveElements(2, 0, 0, 1);
		}

		[Test]
		public void ShouldRemoveLeafFromHighTree()
		{
			AddAndRemoveElements(7, 0, 5);
		}

		[Test]
		public void ShouldRemoveNonLeafFromHighTree()
		{
			AddAndRemoveElements(7, 0, 1);
		}

		[Test]
		public void ShouldRemoveRootFromHighTree()
		{
			AddAndRemoveElements(7, 0, 0);
		}

		[Test]
		public void ShouldRemoveRootFromHighTreeAndAddTwoElements()
		{
			AddAndRemoveElements(7, 2, 0);
		}

		[Test]
		public void ShouldRemoveSomeNonLeavesAndAddElemetsAfter()
		{
			AddAndRemoveElements(7, 3, 1, 2);
		}

		[Test]
		public void ShouldRemoveLeaveAndNonLeafAndAddElemetsAfter()
		{
			AddAndRemoveElements(7, 5, 2, 4);
		}

		private void AddAndRemoveElements(int addBeforeRemove, int addAfterRemove, params int[] indicesToRemove)
		{
			List<RandomAccessBinaryTree<int>.TreeNode> nodes = new List<RandomAccessBinaryTree<int>.TreeNode>();
			RandomAccessBinaryTree<int> tree = new RandomAccessBinaryTree<int>();

			for(int i=0; i < addBeforeRemove; i++)
			{
				int val = 1 << i;
				var node = tree.Add(val);
				nodes.Add(node);
			}

			int removeMask = 0;
			foreach(int i in indicesToRemove)
			{
				removeMask |= nodes[i].data;
				tree.Remove(nodes[i]);
			}

			for (int i = addBeforeRemove; i < addBeforeRemove + addAfterRemove; i++)
			{
				tree.Add(1 << i);
			}

			var collection = tree.GetRandomElements(int.MaxValue);
			int rndCount = 0;
			int res = 0;
			foreach(var item in collection)
			{
				rndCount++;
				res |= item;
			}

			Assert.AreEqual(addBeforeRemove + addAfterRemove - indicesToRemove.Length, rndCount);
			Assert.AreEqual((Convert.ToInt32(Math.Pow(2, addBeforeRemove+addAfterRemove)) - 1) ^ (removeMask), res);
		}
	}
}
