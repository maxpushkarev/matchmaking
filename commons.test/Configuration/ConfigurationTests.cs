﻿using NUnit.Framework;
using System.Threading;
using System.Threading.Tasks;

namespace commons.test
{
	[TestFixture]
	internal sealed class ConfigurationTests
	{
		[Test]
		public void ShouldGenerateTheSameConfigInstance()
		{
			TestConfiguration t1 = ConfigurationFactory<TestConfiguration>.Get();
			TestConfiguration t2 = ConfigurationFactory<TestConfiguration>.Get();
			Assert.AreEqual(t1, t2);
		}

		[Test]
		[Repeat(100)]
		public void ShouldCreateOnlyOneConfigInstance()
		{
			using (Task t1 = Task.Factory.StartNew(() => ConfigurationFactory<TestConfiguration>.Get()))
			using (Task t2 = Task.Factory.StartNew(() => ConfigurationFactory<TestConfiguration>.Get()))
			{
				Task.WaitAll(t1, t2);
				Assert.AreEqual(TestConfiguration.cstrCallsCount, 1);
			}
		}

		private class TestConfiguration : BaseConfiguration
		{
			public static int cstrCallsCount;

			public TestConfiguration()
			{
				Interlocked.Increment(ref cstrCallsCount);
			}
		}
	}
}
