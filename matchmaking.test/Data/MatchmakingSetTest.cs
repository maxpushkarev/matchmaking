﻿using NUnit.Framework;
using server.core;
using System;
using System.Collections.Generic;

namespace matchmaking.test
{
	[TestFixture]
	internal sealed class MatchmakingSetTest
	{
		[Test]
		public void SizeOfSingleSetEqualsOne()
		{
			Assert.AreEqual(1, CreateMatchmakingSet().Size);
		}

		[Test]
		public void ShouldNotIncreaseSizeWhenSelfUnion()
		{
			MatchmakingSet set = CreateMatchmakingSet();
			MatchmakingSet res = set + set;
			Assert.AreEqual(1, res.Size);
			Assert.AreSame(set, res);
		}

		[Test]
		public void ShouldIncreaseSizeWhenAttach()
		{
			MatchmakingSet set1 = CreateMatchmakingSet();
			MatchmakingSet set2 = CreateMatchmakingSet();
			set1 += set2;
			Assert.AreEqual(2, set2.Size);
			Assert.AreEqual(2, set1.Size);
		}

		[Test]
		public void ShouldCorrectAttachAndChangeSize()
		{
			MatchmakingSet set1 = CreateMatchmakingSet();
			MatchmakingSet set2 = CreateMatchmakingSet();
			MatchmakingSet set3 = CreateMatchmakingSet();
			set2 += set3;
			set1 += set2;

			Assert.AreSame(set1, set3.FindSet());
			MatchmakingSet union = set1 + set3;
			Assert.AreSame(set1, union);

			Assert.AreEqual(3, set1.Size);
			Assert.AreEqual(3, set2.Size);
			Assert.AreEqual(3, set3.Size);
		}

		[Test]
		public void ShouldBeIteratedCorrectlyByAnyMemberOfSet()
		{
			MatchmakingSet set1 = CreateMatchmakingSet();
			MatchmakingSet set2 = CreateMatchmakingSet();
			MatchmakingSet set3 = CreateMatchmakingSet();
			set2 += set3;
			set1 += set2;
			set1 += set3;

			AssertMMSetCollectionByInputSet(set1, set1, set2, set3);
			AssertMMSetCollectionByInputSet(set2, set1, set2, set3);
			AssertMMSetCollectionByInputSet(set3, set1, set2, set3);
		}

		[Test]
		public void ShouldBeIteratedCorrectlyAfterUnionOfTwoBigSets()
		{
			MatchmakingSet set1 = CreateMatchmakingSet();
			MatchmakingSet set2 = CreateMatchmakingSet();
			MatchmakingSet set3 = CreateMatchmakingSet();
			set1 += set2;
			set1 += set3;

			MatchmakingSet set4 = CreateMatchmakingSet();
			MatchmakingSet set5 = CreateMatchmakingSet();
			MatchmakingSet set6 = CreateMatchmakingSet();
			set4 += set5;
			set4 += set6;

			set1 += set4;

			AssertMMSetCollectionByInputSet(set1, set1, set2, set3, set4, set5, set6);
		}

		[Test]
		public void ShouldBeCorrectlyIteratedWhenInnerEnumeration()
		{
			MatchmakingSet set1 = CreateMatchmakingSet();
			MatchmakingSet set2 = CreateMatchmakingSet();
			MatchmakingSet set3 = CreateMatchmakingSet();
			set1 += set2;
			set1 += set3;

			int counter = 0;

			foreach(MatchmakingSet set in set1)
			{
				foreach (MatchmakingSet _set in set2)
				{
					counter++;
				}
			}

			Assert.AreEqual(9, counter);
		}

		[Test]
		public void ShouldExtractRootCorrectly()
		{
			MatchmakingSet set1 = CreateMatchmakingSet();
			MatchmakingSet set2 = CreateMatchmakingSet();
			set1 += set2;
			MatchmakingSet prevSet;
			set1.ExtractSingle(out prevSet);
			Assert.AreEqual(1, set1.Size);
			Assert.AreSame(set2, set2.FindSet());
		}

		[Test]
		public void ShouldExtractRootCorrectlyFromBigSet()
		{
			MatchmakingSet set1 = CreateMatchmakingSet();
			MatchmakingSet set2 = CreateMatchmakingSet();
			MatchmakingSet set3 = CreateMatchmakingSet();
			MatchmakingSet set4 = CreateMatchmakingSet();
			MatchmakingSet set5 = CreateMatchmakingSet();
			set1 += set2;
			set1 += set3;
			set4 += set5;
			set1 += set4;
			Assert.AreEqual(5, set1.Size);
			Assert.AreSame(set1, set1.FindSet());
			MatchmakingSet prevSet;
			set1.ExtractSingle(out prevSet);
			AssertMMSetCollectionByInputSet(set1, set1);
			AssertMMSetCollectionByInputSet(set2, set2, set3, set4, set5);
		}

		[Test]
		public void ShouldNotExtractSelf()
		{
			MatchmakingSet set1 = CreateMatchmakingSet();
			set1 += set1;
			MatchmakingSet prevSet;
			set1.ExtractSingle(out prevSet);
			Assert.AreEqual(1, set1.Size);
			Assert.AreSame(set1, set1.FindSet());
		}

		[Test]
		public void ShouldExctractItemWithoutChildIfExists()
		{
			MatchmakingSet set1 = CreateMatchmakingSet();
			MatchmakingSet set2 = CreateMatchmakingSet();
			MatchmakingSet set3 = CreateMatchmakingSet();
			MatchmakingSet set4 = CreateMatchmakingSet();
			set1 += set2;
			set3 += set4;
			set1 += set3;

			MatchmakingSet prevSet;
			set3.ExtractSingle(out prevSet);

			Assert.AreEqual(1, set3.Size);
			Assert.AreSame(set3, set3.FindSet());
			Assert.AreEqual(3, set1.Size);
			Assert.AreSame(set1, set4.FindSet());
			AssertMMSetCollectionByInputSet(set3, set3);
			AssertMMSetCollectionByInputSet(set1, set1, set2, set4);
		}


		[Test]
		public void ShouldExctractItemWithoutChildIfExistsRecursively()
		{
			MatchmakingSet set1 = CreateMatchmakingSet(1);
			MatchmakingSet set2 = CreateMatchmakingSet(2);
			MatchmakingSet set3 = CreateMatchmakingSet(3);
			MatchmakingSet set4 = CreateMatchmakingSet(4);
			MatchmakingSet set5 = CreateMatchmakingSet(5);
			MatchmakingSet set6 = CreateMatchmakingSet(6);

			set2 += set3;
			set2 += set1;
			
			set5 += set6;
			set5 += set4;

			set2 += set5;

			MatchmakingSet prevSet;
			set5.ExtractSingle(out prevSet);

			Assert.AreEqual(1, set5.Size);
			Assert.AreSame(set5, set5.FindSet());
			Assert.AreEqual(5, set1.Size);

			AssertMMSetCollectionByInputSet(set5, set5);
			AssertMMSetCollectionByInputSet(set1, set1, set2, set3, set4, set6);
		}

		[Test]
		public void ShouldMinAndMaxRankBeEqualWhenSingleSetCreated()
		{
			int rank = 3;
			MatchmakingSet set =  CreateMatchmakingSet(rank: 3);
			Assert.AreEqual(rank, set.MaxRank);
			Assert.AreEqual(rank, set.MinRank);
		}

		[Test]
		public void ShouldMinAndMaxRankBeEqualWhenSingleSetExtracted()
		{
			int rank = 3;
			MatchmakingSet set = CreateMatchmakingSet(rank: 3);
			MatchmakingSet prevSet;
			set.ExtractSingle(out prevSet);
			Assert.AreEqual(rank, set.MaxRank);
			Assert.AreEqual(rank, set.MinRank);
		}

		[Test]
		public void ShouldRecalculateMinMaxRankWhenUnion()
		{
			MatchmakingSet set1 = CreateMatchmakingSet(rank: 5);
			MatchmakingSet set2 = CreateMatchmakingSet(rank: 2);
			set1 += set2;
			Assert.AreEqual(5, set1.MaxRank);
			Assert.AreEqual(2, set2.MinRank);
		}


		[Test]
		public void ShouldRecalculateMinMaxRankWhenUnionBigSets()
		{
			MatchmakingSet set1 = CreateMatchmakingSet(rank: 5);
			MatchmakingSet set2 = CreateMatchmakingSet(rank: 2);
			MatchmakingSet set3 = CreateMatchmakingSet(rank: 1);
			MatchmakingSet set4 = CreateMatchmakingSet(rank: 17);

			set1 += set2;
			set3 += set4;

			set1 += set3;

			Assert.AreEqual(17, set4.MaxRank);
			Assert.AreEqual(1, set2.MinRank);
		}


		[Test]
		public void ShouldRecalculateRanksAfterExtractSingle()
		{
			MatchmakingSet set1 = CreateMatchmakingSet(rank: 5);
			MatchmakingSet set2 = CreateMatchmakingSet(rank: 2);

			set1 += set2;
			MatchmakingSet prevSet;
			set2.ExtractSingle(out prevSet);

			Assert.AreEqual(2, set2.MaxRank);
			Assert.AreEqual(2, set2.MinRank);
			Assert.AreEqual(5, set1.MinRank);
			Assert.AreEqual(5, set1.MaxRank);
		}

		[Test]
		public void ShouldRecalculateRanksAfterExtractSingleFromBigSet()
		{
			MatchmakingSet set1 = CreateMatchmakingSet(rank: 5);
			MatchmakingSet set2 = CreateMatchmakingSet(rank: 2);
			MatchmakingSet set3 = CreateMatchmakingSet(rank: 22);
			MatchmakingSet set4 = CreateMatchmakingSet(rank: 8);

			set1 += set2;
			set3 += set4;
			set1 += set3;

			MatchmakingSet prevSet;
			set3.ExtractSingle(out prevSet);

			Assert.AreEqual(22, set3.MaxRank);
			Assert.AreEqual(22, set3.MinRank);

			Assert.AreEqual(2, set4.MinRank);
			Assert.AreEqual(8, set2.MaxRank);
		}

		[Test]
		public void CanBeIterableThroughEnumeratorMoveNext()
		{
			MatchmakingSet set1 = CreateMatchmakingSet(id: 0);
			MatchmakingSet set2 = CreateMatchmakingSet(id: 1);
			MatchmakingSet set3 = CreateMatchmakingSet(id: 2);
			MatchmakingSet set4 = CreateMatchmakingSet(id: 3);

			set1 += set2;
			set3 += set4;
			set1 += set3;

			var enumerator = set4.GetEnumerator();
			int count = 0;
			int res = 0;
			while(enumerator.MoveNext())
			{
				count++;
				res |= (1 << Convert.ToInt32(enumerator.Current.value.user.id));
			}

			Assert.AreEqual(4, count);
			Assert.AreEqual(15, res);
		}

		[Test]
		public void ShouldSetBeGCCollectedWhenExtracted()
		{
			MatchmakingSet set1 = CreateMatchmakingSet(1,1);
			WeakReference wr = null;

			(new Action(() => {
				MatchmakingSet set2 = CreateMatchmakingSet(2,2);
				set1 += set2;
				wr = new WeakReference(set2);
				MatchmakingSet prevSet;
				set2.ExtractSingle(out prevSet);
			}))();

			GC.Collect();
			Assert.Null(wr.Target);
		}

		[Test]
		public void ShouldInitVersion()
		{
			MatchmakingSet set = CreateMatchmakingSet();
			Assert.Zero(set.Version);
		}

		[Test]
		public void ShouldIncrementVersionAfterUnion()
		{
			MatchmakingSet set1 = CreateMatchmakingSet();
			MatchmakingSet set2 = CreateMatchmakingSet();
			set1 += set2;
			Assert.AreEqual(1, set1.Version);
			Assert.AreEqual(1, set2.Version);
		}

		[Test]
		public void ShouldSizeBeOneAfterExtract()
		{
			MatchmakingSet set1 = CreateMatchmakingSet();
			MatchmakingSet set2 = CreateMatchmakingSet();
			set1 += set2;
			Assert.AreEqual(2, set1.Size);
			Assert.AreEqual(2, set2.Size);
			MatchmakingSet prevSet;
			set1.ExtractSingle(out prevSet);
			Assert.AreEqual(1, set1.Size);
			Assert.AreEqual(1, set2.Size);
		}

		[Test]
		public void ShouldSizeBeOneAfterExtractFromBigSet()
		{
			MatchmakingSet set1 = CreateMatchmakingSet();
			MatchmakingSet set2 = CreateMatchmakingSet();
			MatchmakingSet set3 = CreateMatchmakingSet();
			MatchmakingSet set4 = CreateMatchmakingSet();
			set1 += set2;
			set1 += set3;
			set1 += set4;
			MatchmakingSet prevSet;
			set1.ExtractSingle(out prevSet);
			Assert.AreEqual(1, set1.Size);
			Assert.AreEqual(3, set2.Size);
			Assert.AreEqual(3, set3.Size);
			Assert.AreEqual(3, set4.Size);
		}

		[Test]
		public void ShouldIncrementVersionAfterExtract()
		{
			MatchmakingSet set1 = CreateMatchmakingSet();
			MatchmakingSet set2 = CreateMatchmakingSet();
			MatchmakingSet set3 = CreateMatchmakingSet();
			MatchmakingSet set4 = CreateMatchmakingSet();
			MatchmakingSet set5 = CreateMatchmakingSet();
			set1 += set2;
			set1 += set3;
			set4 += set5;
			set1 += set4;
			Assert.AreEqual(3, set1.Version);
			Assert.AreEqual(3, set2.Version);
			Assert.AreEqual(3, set3.Version);
			Assert.AreEqual(3, set4.Version);
			Assert.AreEqual(3, set5.Version);

			MatchmakingSet prevSet;
			set5.ExtractSingle(out prevSet);
			Assert.AreEqual(4, set1.Version);
			Assert.AreEqual(4, set2.Version);
			Assert.AreEqual(4, set3.Version);
			Assert.AreEqual(4, set4.Version);
			Assert.AreEqual(4, set5.Version);

		}

		[Test]
		public void ShouldIncrementVersionWhenExtractRoot()
		{
			MatchmakingSet set1 = CreateMatchmakingSet();
			MatchmakingSet set2 = CreateMatchmakingSet();
			MatchmakingSet set3 = CreateMatchmakingSet();

			set1 += set2;
			set1 += set3;

			MatchmakingSet prevSet;
			set1.ExtractSingle(out prevSet);
			Assert.AreEqual(3, set1.Version);
			Assert.AreEqual(3, set2.Version);
			Assert.AreEqual(3, set3.Version);
		}

		[Test]
		public void ShouldNotIncrementVersionWhenExtractSingleRoot()
		{
			MatchmakingSet set = CreateMatchmakingSet();
			MatchmakingSet prevSet;
			set.ExtractSingle(out prevSet);
			Assert.Zero(set.Version);
		}

		[Test]
		public void ShouldNotIncrementVersionWhenUnionSelf()
		{
			MatchmakingSet set = CreateMatchmakingSet();
			set += set;
			Assert.Zero(set.Version);
		}

		[Test]
		public void ShouldBeCorrectRefToOldSetAfterExtractSingleFromSelf()
		{
			MatchmakingSet set = CreateMatchmakingSet();
			MatchmakingSet prevSet;
			set.ExtractSingle(out prevSet);
			Assert.AreSame(set, prevSet);
		}

		[Test]
		public void ShouldBeCorrectRefToOldSetAfterExtractSingleRoot()
		{
			MatchmakingSet set1 = CreateMatchmakingSet();
			MatchmakingSet set2 = CreateMatchmakingSet();
			MatchmakingSet set3 = CreateMatchmakingSet();

			set1 += set2;
			set1 += set3;

			MatchmakingSet prevSet;
			set1.ExtractSingle(out prevSet);
			Assert.AreSame(set2, prevSet);
		}


		[Test]
		public void ShouldBeCorrectRefToOldSetAfterExtractSingleNonRoot()
		{
			MatchmakingSet set1 = CreateMatchmakingSet();
			MatchmakingSet set2 = CreateMatchmakingSet();
			MatchmakingSet set3 = CreateMatchmakingSet();

			set1 += set2;
			set1 += set3;

			MatchmakingSet prevSet;
			set3.ExtractSingle(out prevSet);
			Assert.AreSame(set1, prevSet);
		}

		[Test]
		public void ShouldBeCorrectRefToOldSetAfterExtractSingleFromBigSet()
		{
			MatchmakingSet set1 = CreateMatchmakingSet();
			MatchmakingSet set2 = CreateMatchmakingSet();
			MatchmakingSet set3 = CreateMatchmakingSet();

			MatchmakingSet set4 = CreateMatchmakingSet();
			MatchmakingSet set5 = CreateMatchmakingSet();
			MatchmakingSet set6 = CreateMatchmakingSet();

			set1 += set2;
			set1 += set3;

			set4 += set5;
			set4 += set6;

			set1 += set4;

			MatchmakingSet prevSet1;
			MatchmakingSet prevSet2;
			set1.ExtractSingle(out prevSet1);
			set6.ExtractSingle(out prevSet2);
			Assert.AreSame(set2, prevSet1);
			Assert.AreSame(set2, prevSet2);
		}

		[Test]
		public void CanExtractAllElemetsWhileIterating()
		{
			MatchmakingSet set1 = CreateMatchmakingSet(1);
			MatchmakingSet set2 = CreateMatchmakingSet(2);
			MatchmakingSet set3 = CreateMatchmakingSet(3);

			MatchmakingSet set4 = CreateMatchmakingSet(4);
			MatchmakingSet set5 = CreateMatchmakingSet(5);
			MatchmakingSet set6 = CreateMatchmakingSet(6);

			set1 += set2;
			set1 += set3;

			set4 += set5;
			set4 += set6;

			set1 += set4;

			int count = 0;
			foreach(MatchmakingSet set in set2)
			{
				MatchmakingSet stub;
				set.ExtractSingle(out stub);
				count++;
			}

			Assert.AreEqual(6, count);

			Assert.AreEqual(1, set1.Size);
			Assert.AreEqual(1, set2.Size);
			Assert.AreEqual(1, set3.Size);
			Assert.AreEqual(1, set4.Size);
			Assert.AreEqual(1, set5.Size);
			Assert.AreEqual(1, set6.Size);

			Assert.AreSame(set1, set1.FindSet());
			Assert.AreSame(set2, set2.FindSet());
			Assert.AreSame(set3, set3.FindSet());
			Assert.AreSame(set4, set4.FindSet());
			Assert.AreSame(set5, set5.FindSet());
			Assert.AreSame(set6, set6.FindSet());
		}

		[Test]
		public void ShouldAttachLessSizedMMSetToMoreSized()
		{
			MatchmakingSet set1 = CreateMatchmakingSet(1);
			MatchmakingSet set2 = CreateMatchmakingSet(2);
			MatchmakingSet set3 = CreateMatchmakingSet(3);
			MatchmakingSet set4 = CreateMatchmakingSet(4);

			MatchmakingSet set5 = CreateMatchmakingSet(5);
			MatchmakingSet set6 = CreateMatchmakingSet(6);

			set1 += set2;
			set1 += set3;
			set1 += set4;

			set5 += set6;
			MatchmakingSet stub;
			set1.ExtractSingle(out stub);

			MatchmakingSet union = set1 + set5;
			Assert.AreSame(set5, set1.FindSet());
			Assert.AreSame(set5, union);
		}

		[Test]
		public void ShouldAliveBeCorrectedForSingleSet()
		{
			MatchmakingSet set = CreateMatchmakingSet();		
			Assert.IsTrue(set.Alive);
			set.Alive = false;
			Assert.IsFalse(set.Alive);

		}

		[Test]
		public void ShouldAliveBeCorrectedForUnion()
		{
			MatchmakingSet set1 = CreateMatchmakingSet();
			MatchmakingSet set2 = CreateMatchmakingSet();
			set1 += set2;
			Assert.IsTrue(set1.Alive);
			Assert.IsTrue(set2.Alive);
			set1.Alive = false;
			Assert.IsFalse(set1.Alive);
			Assert.IsFalse(set2.Alive);
			set2.Alive = true;
			Assert.IsTrue(set1.Alive);
			Assert.IsTrue(set2.Alive);
		}

		[Test]
		public void ShouldAliveBeCorrectedForBigSet()
		{
			MatchmakingSet set1 = CreateMatchmakingSet();
			MatchmakingSet set2 = CreateMatchmakingSet();
			MatchmakingSet set3 = CreateMatchmakingSet();
			MatchmakingSet set4 = CreateMatchmakingSet();
			MatchmakingSet set5 = CreateMatchmakingSet();
			MatchmakingSet set6 = CreateMatchmakingSet();

			set1 += set2;
			set1 += set3;

			set4 += set5;
			set4 += set6;

			set1.Alive = false;

			set1 += set4;

			Assert.IsFalse(set6.Alive);
			set1.Alive = true;
			Assert.IsTrue(set6.Alive);
		}

		[Test]
		public void ShouldAliveBeCorrectedAfterExtract()
		{
			MatchmakingSet set = CreateMatchmakingSet();
			set.Alive = false;
			MatchmakingSet remains;
			set.ExtractSingle(out remains);
			Assert.False(set.Alive);
			Assert.False(remains.Alive);
		}

		[Test]
		public void ShouldAliveBeCorrectedAfterExtractRoot()
		{
			MatchmakingSet set1 = CreateMatchmakingSet();
			MatchmakingSet set2 = CreateMatchmakingSet();
			MatchmakingSet set3 = CreateMatchmakingSet();

			set1 += set2;
			set1 += set3;

			set3.Alive = false;

			MatchmakingSet remaining;
			set1.ExtractSingle(out remaining);

			Assert.False(set1.Alive);
			Assert.False(set2.Alive);
			Assert.False(set3.Alive);
			Assert.False(remaining.Alive);
		}

		[Test]
		public void ShouldAliveBeCorrectedAfterExtractNonRoot()
		{
			MatchmakingSet set1 = CreateMatchmakingSet();
			MatchmakingSet set2 = CreateMatchmakingSet();
			MatchmakingSet set3 = CreateMatchmakingSet();

			set1 += set2;
			set1 += set3;

			set1.Alive = false;

			MatchmakingSet remaining;
			set2.ExtractSingle(out remaining);

			Assert.False(set1.Alive);
			Assert.False(set2.Alive);
			Assert.False(set3.Alive);
			Assert.False(remaining.Alive);
		}

		private void AssertMMSetCollectionByInputSet(MatchmakingSet set, params MatchmakingSet[] setsToCheck)
		{
			List<MatchmakingSet> sets = new List<MatchmakingSet>();
			foreach (MatchmakingSet _set in set)
			{
				sets.Add(_set);
			}

			Assert.AreEqual(setsToCheck.Length, sets.Count);
			Array.ForEach(setsToCheck, s => Assert.IsTrue(sets.Contains(s)));
		}

		private MatchmakingSet CreateMatchmakingSet(long id = 0L, int rank = 0)
		{
			return new MatchmakingSet(new MatchmakingUser(new User(id, rank)));
		}
	}
}
