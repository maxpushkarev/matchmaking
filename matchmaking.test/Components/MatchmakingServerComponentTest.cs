﻿using commons;
using NUnit.Framework;
using server.core;
using System;
using System.Linq;
using System.Threading;

namespace matchmaking.test
{
	[TestFixture]
	internal sealed class MatchmakingServerComponentTest
	{
		private const int REPEAT_COUNT = 13;

		private TestMMConfiguration mmCfg;
		private TestCommonConfiguration commonCfg;
		
		[SetUp]
		public void SetUp()
		{
			mmCfg = new TestMMConfiguration();
			commonCfg = new TestCommonConfiguration();
		}

		[Test]
		public void ShouldRemoveUserWithoutMemoryLeak()
		{
			WeakReference wrUser = null;
			WeakReference wrContext = null;
			AutoResetEvent evt = new AutoResetEvent(false);
			MatchmakingWaitForAddedUser matchmaking = new MatchmakingWaitForAddedUser(evt);
			using (matchmaking)
			using (evt)
			{
				matchmaking.Start();
				(new Action(() =>
				{

					User user;
					CrossDomainCommandContext context;
					matchmaking.AddUserToMM(1, 1, out user, out context);
					wrUser = new WeakReference(user);
					wrContext = new WeakReference(context);
					evt.WaitOne();
					matchmaking.ExitUser(context);
				}))();

				matchmaking.PutStopCommand();
				matchmaking.Wait();
				GC.Collect();
			}
			Assert.Null(wrUser.Target);
			Assert.Null(wrContext.Target);
		}

		[Test]
		public void ShouldPassWhenDoubleRemoving()
		{
			MatchmakingExceptionServerComponent matchmaking = new MatchmakingExceptionServerComponent();
			using (matchmaking)
			{
				matchmaking.Start();
				User user;
				CrossDomainCommandContext context;
				matchmaking.AddUserToMM(1, 1, out user, out context);
				matchmaking.ExitUser(context);
				matchmaking.ExitUser(context);
				matchmaking.PutStopCommand();
				matchmaking.Wait();
			}

			Assert.Null(matchmaking.Exception);
		}

		[Test]
		public void ShouldMatchSameRanks()
		{
			CountdownEvent evt = new CountdownEvent(1);
			MatchmakingMatchSignalingComponent matchmaking = new MatchmakingMatchSignalingComponent(evt);
			using (evt)
			using (matchmaking)
			{
				matchmaking.Start();
				for(int i=0; i < mmCfg.UsersInMatchCount; i++)
				{
					matchmaking.AddUserToMM(1, commonCfg.MaxUserRank);
				}
				evt.Wait();
			}

			Assert.Pass();
		}

		[Test]
		[Repeat(REPEAT_COUNT)]
		public void ShouldMatchDifferentRanks()
		{
			CountdownEvent evt = new CountdownEvent(1);
			MatchmakingMatchSignalingComponent matchmaking = new MatchmakingMatchSignalingComponent(evt);
			using (evt)
			using (matchmaking)
			{
				matchmaking.Start();
				for (int i = 0; i < mmCfg.UsersInMatchCount; i++)
				{
					matchmaking.AddUserToMM(1, i+1);
				}
				evt.Wait();
			}

			Assert.Pass();
		}

		[Test]
		[Repeat(REPEAT_COUNT)]
		public void ShouldMatchDifferentRankGroupsIntoDifferentMatches()
		{
			CountdownEvent evt = new CountdownEvent(2);
			MatchmakingMatchSignalingComponent matchmaking = new MatchmakingMatchSignalingComponent(evt);
			using (evt)
			using (matchmaking)
			{
				matchmaking.Start();

				for(int i=0; i < mmCfg.UsersInMatchCount; i++)
				{
					matchmaking.AddUserToMM(i, commonCfg.MinUserRank);
				}

				for (int i = 0; i < mmCfg.UsersInMatchCount; i++)
				{
					matchmaking.AddUserToMM(i, commonCfg.MaxUserRank);
				}

				evt.Wait();
			}

			Assert.IsTrue(matchmaking.matches[0].matchedUsers.All(mu => mu.user.rank == commonCfg.MinUserRank));
			Assert.IsTrue(matchmaking.matches[1].matchedUsers.All(mu => mu.user.rank == commonCfg.MaxUserRank));
		}

		[Test]
		[Repeat(REPEAT_COUNT)]
		public void ShouldMatchRandomRankedWhenSomeUserExits()
		{
			CountdownEvent evt = new CountdownEvent(1);
			MatchmakingMatchSignalingComponent matchmaking = new MatchmakingMatchSignalingComponent(evt);
			Random rnd = new Random();

			int firstPlayersSetCount = mmCfg.UsersInMatchCount - 1;
			User[] users = new User[firstPlayersSetCount];
			CrossDomainCommandContext[] contexts = new CrossDomainCommandContext[firstPlayersSetCount];
			int exitsCount = Math.Min(2, firstPlayersSetCount);
			int[] exitIndices = new int[exitsCount];

			using (evt)
			using (matchmaking)
			{
				matchmaking.Start();

				for (int i = 0; i < firstPlayersSetCount; i++)
				{
					
					matchmaking.AddUserToMM(
						i, 
						Commons.RndRange(
							rnd, 
							commonCfg.MinUserRank, 
							commonCfg.MaxUserRank+1
						),
						out users[i],
						out contexts[i]
					);
				}

				for(int i=0; i<exitsCount; i++)
				{
					int index = Commons.RndRange(rnd, 0, firstPlayersSetCount);
					exitIndices[i] = index;
					matchmaking.ExitUser(contexts[index]);
				}

				for(int i=firstPlayersSetCount; i< firstPlayersSetCount + exitsCount + 1; i++)
				{
					matchmaking.AddUserToMM(
						i,
						Commons.RndRange(
							rnd,
							commonCfg.MinUserRank,
							commonCfg.MaxUserRank + 1
						)
					);
				}

				evt.Wait();
			}

			Assert.IsTrue(matchmaking.matches[0].matchedUsers.All(
				mu => exitIndices.All(i => users[i].id != mu.user.id)
			));
		}
	}
}
