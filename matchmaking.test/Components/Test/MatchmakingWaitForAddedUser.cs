﻿using System.Threading;
using server.core;

namespace matchmaking.test
{
	internal sealed class MatchmakingWaitForAddedUser : TestMatchmakingServerComponent
	{
		private AutoResetEvent evt;
		public MatchmakingWaitForAddedUser(AutoResetEvent evt) : base()
		{
			this.evt = evt;
		}

		protected override void ExecuteEnterMatchmaking(ContextConnectedExecutableCommand<User, CrossDomainCommandContext> cmd)
		{
			base.ExecuteEnterMatchmaking(cmd);
			evt.Set();
		}

		internal override void UpdateUserInMM(MatchmakingUserWrapper mmUserWrapper)
		{
		}
	}
}
