﻿using System;
using System.Threading;

namespace matchmaking.test
{
	internal sealed class MatchmakingExceptionServerComponent : TestMatchmakingServerComponent
	{
		private volatile Exception exception;
		public Exception Exception { get { return exception; } }

		protected override void Run()
		{
			try
			{
				base.Run();
			}
			catch (Exception ex)
			{
				if(ex is ThreadAbortException)
				{
					return;
				}
				exception = ex;
			}
		}
	}
}
