﻿using server.core;

namespace matchmaking.test
{
	internal class TestMatchmakingServerComponent : MatchmakingServerComponent<TestMMConfiguration, TestCommonConfiguration>
	{
		public TestMatchmakingServerComponent() : base(string.Empty, null)
		{
		}

		[ExecuteCommandHandler]
		protected void OnStop(StopExecutableCommand cmd)
		{
			Stop();
		}

		public void PutStopCommand()
		{
			PutCommand(new StopExecutableCommand());
		}

		public void AddUserToMM(long id, int rank, out User user, out CrossDomainCommandContext context)
		{
			user = new User(id, rank);
			context = new CrossDomainCommandContext();
			PutCommand(new ContextConnectedExecutableCommand<User, CrossDomainCommandContext>(user, context));
		}

		public void AddUserToMM(long id, int rank)
		{
			User user = new User(id, rank);
			CrossDomainCommandContext context = new CrossDomainCommandContext();
			PutCommand(new ContextConnectedExecutableCommand<User, CrossDomainCommandContext>(user, context));
		}

		public void ExitUser(CrossDomainCommandContext context)
		{
			PutCommand(context, new ContextDisconnectedExecutableCommand());
		}
	}
}
