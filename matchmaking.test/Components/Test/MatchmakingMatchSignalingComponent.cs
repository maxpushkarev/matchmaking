﻿using System.Collections.Generic;
using System.Threading;

namespace matchmaking.test
{
	internal class MatchmakingMatchSignalingComponent : TestMatchmakingServerComponent
	{
		private CountdownEvent evt;
		internal List<Match> matches;

		internal MatchmakingMatchSignalingComponent(CountdownEvent evt)
		{
			this.evt = evt;
			this.matches = new List<Match>();
		}

		internal override void ExecuteMatchCreatedCommand(MatchCreatedExecutableCommand cmd)
		{
			matches.Add(cmd.Match);
			evt.Signal();
		}
	}
}
