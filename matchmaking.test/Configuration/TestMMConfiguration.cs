﻿using commons;
using System;

namespace matchmaking.test
{
	internal sealed class TestMMConfiguration : BaseConfiguration, IMatchmakingConfiguration
	{
		public Func<int, int, long> WaitTimeFunc => (rank1, rank2) => Math.Abs(rank1 - rank2)*100;
		public Func<int, long, long> UserWeightFunc => (userCount, waitingTime) => (8 - userCount) * 1000 + waitingTime;
		public int UsersInMatchCount => 4;
		public int ImproveUserWeightInterval => 100;
		public int MaxRndSetSelectCount => int.MaxValue;
	}
}
