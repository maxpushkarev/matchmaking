﻿using commons;
using System;

namespace matchmaking.test
{
	internal sealed class TestCommonConfiguration : BaseConfiguration, ICommonConfiguration
	{
		public string Host => throw new NotImplementedException();
		public string Port => throw new NotImplementedException();
		public int MinUserRank => 1;
		public int MaxUserRank => 8;
	}
}
