﻿using NUnit.Framework;
using server.core;
using System;

namespace matchmaking.test
{
	[TestFixture]
	internal sealed class MatchmakingUtilsTest
	{
		[Test]
		public void ShouldCalculateUserWeightWhenSingleSet()
		{
			TestUtilsMatchmakingConfiguration cfg = new TestUtilsMatchmakingConfiguration();
			MatchmakingSet set = CreateMMSet(1);
			Assert.GreaterOrEqual(cfg.UserWeightFunc(1, 0), MatchmakingUtils.CalculateUserWeight(set.value, set, cfg));
		}

		[Test]
		public void ShouldCalculateUserWeightWhenBigSet()
		{
			TestUtilsMatchmakingConfiguration cfg = new TestUtilsMatchmakingConfiguration();
			MatchmakingSet set1 = CreateMMSet(1);
			MatchmakingSet set2 = CreateMMSet(8);
			MatchmakingSet set3 = CreateMMSet(16);
			MatchmakingSet set4 = CreateMMSet(5);
			set1 += set2;
			set1 += set3;
			set1 += set4;
			Assert.GreaterOrEqual(cfg.UserWeightFunc(4, cfg.WaitTimeFunc(8, 16)), MatchmakingUtils.CalculateUserWeight(set2.value, set3, cfg));
		}

		[Test]
		public void ShouldCalculateUserPossibleWeightWhenSingleSet()
		{
			TestUtilsMatchmakingConfiguration cfg = new TestUtilsMatchmakingConfiguration();
			MatchmakingSet set1 = CreateMMSet(1);
			MatchmakingSet set2 = CreateMMSet(2);
			Assert.GreaterOrEqual(cfg.UserWeightFunc(2, cfg.WaitTimeFunc(1, 2)), MatchmakingUtils.CalculatePossibleUserWeight(set1.value, set2, cfg));
		}

		[Test]
		public void ShouldCalculateUserPossibleWeightWhenBigSet()
		{
			TestUtilsMatchmakingConfiguration cfg = new TestUtilsMatchmakingConfiguration();
			MatchmakingSet set1 = CreateMMSet(1);
			MatchmakingSet set2 = CreateMMSet(8);
			MatchmakingSet set3 = CreateMMSet(16);
			MatchmakingSet set4 = CreateMMSet(5);
			MatchmakingSet set5 = CreateMMSet(2);
			set1 += set2;
			set1 += set3;
			set1 += set4;
			Assert.GreaterOrEqual(cfg.UserWeightFunc(5, cfg.WaitTimeFunc(2, 16)), MatchmakingUtils.CalculatePossibleUserWeight(set5.value, set4, cfg));
		}

		[Test]
		public void ShouldCalculateMaximumRemaininWaitingTimeWhenSingleSet()
		{
			TestUtilsMatchmakingConfiguration cfg = new TestUtilsMatchmakingConfiguration();
			MatchmakingSet set1 = CreateMMSet(1);
			Assert.Zero(MatchmakingUtils.GetMaximumRemainingWaitingTime(set1, cfg));
		}

		[Test]
		public void ShouldCalculateMaximumRemaininWaitingTimeWhenBigSet()
		{
			TestUtilsMatchmakingConfiguration cfg = new TestUtilsMatchmakingConfiguration();
			MatchmakingSet set1 = CreateMMSet(1);
			MatchmakingSet set2 = CreateMMSet(8);
			MatchmakingSet set3 = CreateMMSet(16);
			MatchmakingSet set4 = CreateMMSet(5);
			set1 += set2;
			set1 += set3;
			set1 += set4;
			Assert.GreaterOrEqual(cfg.WaitTimeFunc(1,16), MatchmakingUtils.GetMaximumRemainingWaitingTime(set1, cfg));
		}

		[Test]
		public void ShouldCalculateRemainingTimeWithArbitraryRank()
		{
			TestUtilsMatchmakingConfiguration cfg = new TestUtilsMatchmakingConfiguration();
			MatchmakingSet set1 = CreateMMSet(5);
			Assert.GreaterOrEqual(cfg.WaitTimeFunc(5, 18), MatchmakingUtils.GetRemainingWaitingTime(set1.value, 18, cfg));
		}

		private MatchmakingSet CreateMMSet(int rank)
		{
			return new MatchmakingSet(CreateMMUser(rank));
		}

		private MatchmakingUser CreateMMUser(int rank)
		{
			return new MatchmakingUser(new User(0, rank));
		}

		private sealed class TestUtilsMatchmakingConfiguration : IMatchmakingConfiguration
		{
			public Func<int, int, long> WaitTimeFunc => (r1, r2) => Math.Abs(r1 - r2) * 10000;
			public Func<int, long, long> UserWeightFunc => (size, waitTime) => -size + waitTime;
			public int UsersInMatchCount => throw new NotImplementedException();
			public int MaxRndSetSelectCount => throw new NotImplementedException();
			public int ImproveUserWeightInterval => throw new NotImplementedException();
		}

	}
}
