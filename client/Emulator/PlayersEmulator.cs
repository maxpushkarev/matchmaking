﻿using commons;
using Ninject;
using System;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;

namespace client
{
	internal sealed class PlayersEmulator
	{
		private const int DELAY_EMULATION_SEC = 7;
		private Random random = new Random();

		[Inject]
		public ClientConfiguration ClientConfiguration { get; set; }
		[Inject]
		public ICommonConfiguration CommonConfiguration { get; set; }

		public async void Run()
		{
			Console.WriteLine("Wait {0} seconds to begin emulation", DELAY_EMULATION_SEC);
			await Task.Delay(DELAY_EMULATION_SEC * 1000);
			Enumerable.Repeat<Action>(
				async () =>
				{

					while (true)
					{
						await Task.Delay(GetRandomConnectionDelay());

						using (TcpClient client = new TcpClient(CommonConfiguration.Host, int.Parse(CommonConfiguration.Port)))
						using (NetworkStream stream = client.GetStream())
						{
							byte[] buffer = new byte[Commons.BUFFER_LENGTH];
							int bytesRead = await stream.ReadAsync(buffer, 0, Commons.BUFFER_LENGTH);
							ContextExecutableCommand clientCmd = Commons.DeserializeCommand(buffer, bytesRead, new BinaryFormatter());
							if (!(clientCmd is UserAuthorizedExecutableCommand))
							{
								throw new ArgumentException("Invalid cmd from server!");
							}
							Commons.SendCommand<EnterBattleExecutableCommand>(stream);
							await Task.Delay(GetRandomLifetime());
						}
					}
				},
				ClientConfiguration.PlayersCount
			)
			.ToList()
			.ForEach(p => p());
			Console.WriteLine("Press any key to finish emulation...");
		}

		private int GetRandomConnectionDelay()
		{
			return Commons.RndRange(random, ClientConfiguration.ConnectionDelayMin, ClientConfiguration.ConnectionDelayMax);
		}

		private int GetRandomLifetime()
		{
			return Commons.RndRange(random, ClientConfiguration.LifetimeMin, ClientConfiguration.LifetimeMax);
		}
	}
}
