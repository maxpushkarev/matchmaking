﻿using commons;
using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("client.test")]
namespace client
{
	internal sealed class Program
	{
		private static void Main(string[] args)
		{
			CommonInjectorActivator.Bind<ICommonConfiguration>(ConfigurationFactory<CommonConfiguration>.Get());
			CommonInjectorActivator.Bind<ClientConfiguration>(ConfigurationFactory<ClientConfiguration>.Get());
			CommonInjectorActivator.Get<PlayersEmulator>().Run();
			Console.ReadKey();
		}
	}
}