﻿using commons;
using Microsoft.Extensions.Configuration;

namespace client
{
	internal sealed class ClientConfiguration : BaseConfiguration
	{
		public int PlayersCount { get; private set; }
		public int LifetimeMin { get; private set; }
		public int LifetimeMax { get; private set; }
		public int ConnectionDelayMin { get; private set; }
		public int ConnectionDelayMax { get; private set; }

		public ClientConfiguration()
		{
			IConfiguration clientCfg = new ConfigurationBuilder()
				.AddJsonFile("client.json", false, false)
				.Build();

			PlayersCount = int.Parse(clientCfg["playersCount"]);
			LifetimeMin = int.Parse(clientCfg["lifetime:min"]);
			LifetimeMax = int.Parse(clientCfg["lifetime:max"]);
			ConnectionDelayMin = int.Parse(clientCfg["delays:connection:min"]);
			ConnectionDelayMax = int.Parse(clientCfg["delays:connection:max"]);
		}
	}
}
