﻿using commons;
using System;

namespace server.core
{
	[Serializable]
	public sealed class Server2ClientNetworkCommand : ContextExecutableCommand
	{
		public readonly ContextExecutableCommand innerCmd;

		public Server2ClientNetworkCommand(ContextExecutableCommand innerCmd)
		{
			this.innerCmd = innerCmd;
		}
	}
}
