﻿namespace server.core
{
	public sealed class Session : CommandContext
	{
		public readonly long sessionId;

		public Session(long sessionId)
		{
			this.sessionId = sessionId;
		}

		public override string ToString()
		{
			return string.Format("Session: [id = {0}]", sessionId);
		}
	}
}
