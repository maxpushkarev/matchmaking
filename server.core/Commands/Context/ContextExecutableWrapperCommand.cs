﻿using commons;

namespace server.core
{
	public sealed class ContextExecutableWrapperCommand : BaseExecutableCommand
	{
		public readonly CommandContext context;
		public readonly ContextExecutableCommand command;

		public ContextExecutableWrapperCommand(CommandContext context, ContextExecutableCommand command)
		{
			this.context = context;
			this.command = command;
		}
	}
}
