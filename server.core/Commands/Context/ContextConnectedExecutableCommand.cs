﻿using commons;
using System;

namespace server.core
{
	[Serializable]
	public class ContextConnectedExecutableCommand<T> : ContextExecutableCommand where T : CommandContext
	{
		public readonly T context;

		public ContextConnectedExecutableCommand(T context)
		{
			this.context = context;
		}
	}

	[Serializable]
	public sealed class ContextConnectedExecutableCommand<TContext, TContextProxy> 
		: ContextConnectedExecutableCommand<TContext>
			where TContext : CommandContext
			where TContextProxy : CrossDomainCommandContext
	{
		public readonly TContextProxy contextProxy;

		public ContextConnectedExecutableCommand(TContext context, TContextProxy contextProxy) : base(context)
		{
			this.contextProxy = contextProxy;
		}
	}
}
