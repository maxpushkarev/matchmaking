﻿using commons;

namespace server.core
{
	public delegate void ContextCommandExecuteHandler<out TCommand>(ContextExecutableCommand cmd) where TCommand : ContextExecutableCommand;
}
