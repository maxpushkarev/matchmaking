﻿namespace server.core
{
	public sealed class User : CommandContext
	{
		public readonly long id;
		public readonly int rank;

		public User(long id, int rank)
		{
			this.id = id;
			this.rank = rank;
		}

		public override string ToString()
		{
			return string.Format("User:[id = {0} ; rank = {1}]", id, rank);
		}

		public override bool Equals(object obj)
		{
			if(obj == null)
			{
				return false;
			}
			if(!(obj is User))
			{
				return false;
			}

			return Equals((obj as User).id.Equals(id));
		}

		public override int GetHashCode()
		{
			return id.GetHashCode();
		}
	}
}
