﻿using System;
using System.Threading;

namespace server.core
{
	public abstract class ServerComponent : MarshalByRefServerObject, IDisposable
	{
		private const int HASH_CODE_MOD = 113;

		//TODO : think about more gethashcodeimpl
		private int hashCode = DateTime.UtcNow.Millisecond % HASH_CODE_MOD;
		private Thread componentThread;
		private bool disposed = false;

		public AppDomain Domain { get; private set; }
		public string Name { get; private set; }
		protected Thread ComponentThread { get { return componentThread; } }

		protected ServerComponent(string name)
		{
			Name = name;
			Domain = AppDomain.CurrentDomain;
		}

		public virtual void Start()
		{
			componentThread = new Thread(() =>
			{
				Thread.MemoryBarrier();
				while (true)
				{
					Run();
				}
			})
			{
				IsBackground = true
			};
			componentThread.Start();
		}

		public void Stop()
		{
			if(componentThread != null)
			{
				componentThread.Abort();
			}
			Thread.MemoryBarrier();
		}

		public void Wait()
		{
			if (componentThread != null)
			{
				componentThread.Join();
			}
			Thread.MemoryBarrier();
		}

		protected abstract void Run();

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposed)
			{
				return;
			}

			if (disposing)
			{
				DisposeManagedResources();
			}

			disposed = true;
		}

		private void DisposeManagedResources()
		{
			Stop();
		}

		~ServerComponent()
		{
			Dispose(false);
		}

		public override bool Equals(object obj)
		{
			return ReferenceEquals(this, obj);
		}

		public override int GetHashCode()
		{
			return hashCode;
		}
	}
}