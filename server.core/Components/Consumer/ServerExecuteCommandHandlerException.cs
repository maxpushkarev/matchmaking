﻿namespace server.core
{
	internal sealed class ServerExecuteCommandHandlerException : ServerCommandConsumerException
	{
		public ServerExecuteCommandHandlerException(ServerCommandConsumer consumer, string msg) : base(consumer, msg)
		{
		}
	}
}
