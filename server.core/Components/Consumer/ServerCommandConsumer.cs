﻿using commons;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting;
using System.Threading;

namespace server.core
{
	public abstract class ServerCommandConsumer : ServerComponent
	{
		protected static BindingFlags bindings = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;

		private ConditionalWeakTable<CommandContext, Dictionary<Type, ContextCommandExecuteHandler<ContextExecutableCommand>>> context2HandlersMap
			= new ConditionalWeakTable<CommandContext, Dictionary<Type, ContextCommandExecuteHandler<ContextExecutableCommand>>>();
		private Dictionary<Type, MethodInfo> executeHandlers;
		private BlockingCollection<BaseExecutableCommand> executableCommandQueue = 
			new BlockingCollection<BaseExecutableCommand>();
		private object[] commandArgs = new object[1];

		protected ServerCommandConsumer(string name) : base(name)
		{
			IEnumerable<KeyValuePair<Type, MethodInfo>> parsedCommandHandlers =
				GetType()
				.GetMethods(bindings)
				.Where(mi =>
				{
					if (mi.GetCustomAttribute<ExecuteCommandHandlerAttribute>() == null)
					{
						return false;
					}

					ValidateCommandHandler(mi);

					return true;
				})
				.Select(mi => new KeyValuePair<Type, MethodInfo>(mi.GetParameters().First().ParameterType, mi));

			if (parsedCommandHandlers.GroupBy(p => p.Key).Any(g => g.Count() > 1))
			{
				throw new ServerExecuteCommandHandlerException(
					this, string.Format("There are some collisions within command handlers!")
				);
			}

			executeHandlers = parsedCommandHandlers.ToDictionary(p => p.Key, p => p.Value);
		}

		private void ValidateCommandHandler(MethodInfo mi)
		{
			Type returnType = mi.ReturnParameter.ParameterType;

			if (!returnType.Equals(typeof(void)))
			{
				throw new ServerExecuteCommandHandlerException(
					this, string.Format("Method = {0} ; Return param must be void type (actual type = {1})", mi.Name, returnType.Name)
				);
			}

			ParameterInfo[] parameters = mi.GetParameters();

			if (parameters.Length != 1) {
				throw new ServerExecuteCommandHandlerException(
					this, string.Format("Method = {0} ; There must be 1 parameter (actual count = {1})", mi.Name, parameters.Length)
				);
			}

			Array.ForEach<ParameterInfo>(parameters, (pi) => {

				if (!pi.ParameterType.IsSubclassOf(typeof(BaseExecutableCommand)))
				{
					throw new ServerExecuteCommandHandlerException(
						this, string.Format("Method = {0} ; Parameter must be subclass of 'BaseServerExecutableCommand' (actual type = {1})", mi.Name, pi.ParameterType.Name)
					);
				}

				if (pi.ParameterType.IsAbstract)
				{
					throw new ServerExecuteCommandHandlerException(
						this, string.Format("Method = {0} ; Parameter type '{1}' must not be abstract)", mi.Name, pi.ParameterType.Name)
					);
				}

				if (pi.ParameterType.Equals(typeof(ContextExecutableWrapperCommand)))
				{
					throw new ServerExecuteCommandHandlerException(
						this, string.Format("Method = {0} ; Parameter type '{1}' must not be 'ContextExecutableWrapperCommand')", mi.Name, pi.ParameterType.Name)
					);
				}

			});
		}

		protected override void Run()
		{
			BaseExecutableCommand command;
			executableCommandQueue.TryTake(out command, Timeout.Infinite);
			Execute(command);
		}

		public void PutCommand(BaseExecutableCommand baseServerExecutable) {
			executableCommandQueue.TryAdd(baseServerExecutable);
		}

		protected void Execute(BaseExecutableCommand command)
		{
			Type commandType = command.GetType();

			if(commandType.Equals(typeof(ContextExecutableWrapperCommand)))
			{
				//most commands are context commands
				//boost perfomance by direct call of execute method
				ExecuteContextCommand((ContextExecutableWrapperCommand) command);
				return;
			}

			MethodInfo mi;
			if (!executeHandlers.TryGetValue(commandType, out mi)) {
				throw new ServerExecuteCommandHandlerException(
					this,
					string.Format("There is no handler for command '{0}'", commandType.Name)
				);
			}

			commandArgs[0] = command;
			mi.Invoke(this, commandArgs);
		}

		private void ExecuteContextCommand(ContextExecutableWrapperCommand cmdWrapper)
		{
			CommandContext context = cmdWrapper.context;
			Dictionary<Type, ContextCommandExecuteHandler<ContextExecutableCommand>> handlers;

			if (RemotingServices.IsTransparentProxy(context))
			{
				throw new ServerExecuteCommandHandlerException(
					this, string.Format(
						"Context instance of type '{0}' should not be transparent proxy", context.GetType().Name
					)
				);
			}
			
			if(!context2HandlersMap.TryGetValue(context, out handlers))
			{
				return;
			}

			ContextExecutableCommand cmd = cmdWrapper.command;
			Type cmdType = cmd.GetType();
			ContextCommandExecuteHandler<ContextExecutableCommand> handler;

			if(!handlers.TryGetValue(cmdType, out handler))
			{
				throw new ServerExecuteCommandHandlerException(
					this, string.Format(
						"Handler for command type '{0}' not registered for context instance of type '{1}'", cmdType.Name, context.GetType().Name
					)
				);
			}

			handler.Invoke(cmd);
		}

		public void UnregisterContext(CommandContext context)
		{
			if (!context2HandlersMap.Remove(context)) {
				throw new ServerExecuteCommandHandlerException(
					this, string.Format(
						"Unable to unregister unknown context of type '{0}'", context.GetType().Name
					)
				);
			}
		}

		public void RegisterContextExecutableHandler<TCommand>(
			CommandContext context, ContextCommandExecuteHandler<TCommand> cmdHandler
			)
			where TCommand : ContextExecutableCommand
		{

			if (typeof(TCommand).IsAbstract)
			{
				throw new ServerExecuteCommandHandlerException(
					this, string.Format(
						"Command type '{0}' should not be abstract", typeof(TCommand).Name
					)
				);
			}

			Dictionary<Type, ContextCommandExecuteHandler<ContextExecutableCommand>> handlers = context2HandlersMap.GetOrCreateValue(context);

			ContextCommandExecuteHandler<ContextExecutableCommand> handler;
			if(handlers.TryGetValue(typeof(TCommand), out handler))
			{
				handler += cmdHandler;
				handlers[typeof(TCommand)] = handler;
				return;
			}

			handlers.Add(typeof(TCommand), cmdHandler);
		}

		public void PutCommand(CommandContext context, ContextExecutableCommand cmd)
		{
			PutCommand(GenerateContextExecutableCommand(context, cmd));
		}

		protected ContextExecutableWrapperCommand GenerateContextExecutableCommand(CommandContext context, ContextExecutableCommand cmd)
		{
			return new ContextExecutableWrapperCommand(context, cmd);
		}
	}
}
