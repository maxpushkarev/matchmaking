﻿using System;

namespace server.core
{
	[AttributeUsage(AttributeTargets.Method)]
	public sealed class ExecuteCommandHandlerAttribute : Attribute
	{
	}
}
