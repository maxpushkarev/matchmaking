﻿using System;

namespace server.core
{
	public abstract class ServerCommandConsumerException : Exception
	{
		public ServerCommandConsumerException(ServerCommandConsumer command, string msg) :
			base(string.Format("Command component = {0} ; message = {1}", command.GetType().Name, msg))
		{
		}
	}
}
