﻿using System;
using System.Globalization;
using System.Reflection;
using System.Security.Permissions;

namespace server.core
{
	public class MarshalByRefServerObject : MarshalByRefObject
	{
		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.Infrastructure)]
		public override object InitializeLifetimeService()
		{
			return null;
		}

		public static T CreateTransparentProxyObject<T>(AppDomain domain, Type type, params object[] cstrArgs) where T : MarshalByRefServerObject
		{
			return domain.CreateInstanceAndUnwrap(
				Assembly.GetAssembly(type).FullName,
				type.FullName,
				false,
				BindingFlags.Default,
				null,
				cstrArgs,
				CultureInfo.InvariantCulture,
				null
			) as T;
		}

		public static T CreateTransparentProxyObject<T>(AppDomain domain, params object[] cstrArgs) where T : MarshalByRefServerObject
		{
			return CreateTransparentProxyObject<T>(domain, typeof(T), cstrArgs);
		}

	}
}
