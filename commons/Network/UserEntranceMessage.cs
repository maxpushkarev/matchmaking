﻿using System;

namespace commons
{
	[Serializable]
	public struct UserEntranceMessage
	{
		public readonly long id;
		public UserEntranceMessage(long id) : this()
		{
			this.id = id;
		}
	}
}