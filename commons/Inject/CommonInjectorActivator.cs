﻿using Ninject;

namespace commons
{
	public static class CommonInjectorActivator
	{
		private static readonly StandardKernel APP_KERNEL;

		static CommonInjectorActivator() {
			APP_KERNEL = new StandardKernel();
		}

		public static void Bind<T>(T instance)
		{
			APP_KERNEL.Rebind<T>().ToConstant(instance);
		}

		public static T Get<T>()
		{
			return APP_KERNEL.Get<T>();
		}
	}
}
