﻿namespace commons
{
	public abstract class BaseConfiguration
	{
		public static bool operator true(BaseConfiguration cfg)
		{
			return cfg != null;
		}

		public static bool operator false(BaseConfiguration cfg)
		{
			return cfg == null;
		}

		public static bool operator !(BaseConfiguration cfg)
		{
			return cfg == null;
		}
	}
}
