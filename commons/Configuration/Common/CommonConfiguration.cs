﻿using System.IO;
using System.Runtime.Serialization;
using System.Xml;

namespace commons
{
	public sealed class CommonConfiguration : BaseConfiguration, ICommonConfiguration
	{
		private CommonCfg commonCfg;

		public CommonConfiguration() {

			using (FileStream fs = new FileStream("commons.xml", FileMode.Open))
			using (XmlDictionaryReader reader =
				XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas())
			)
			{
				DataContractSerializer serializer = new DataContractSerializer(typeof(CommonCfg));
				commonCfg = serializer.ReadObject(reader) as CommonCfg;
			}

		}

		public string Host
		{
			get
			{
				return commonCfg.Host;
			}
		}

		public string Port
		{
			get
			{
				return commonCfg.Port;
			}
		}

		public int MinUserRank
		{
			get
			{
				return commonCfg.User.MinRank;
			}
		}

		public int MaxUserRank
		{
			get
			{
				return commonCfg.User.MaxRank;
			}
		}
	}
}
