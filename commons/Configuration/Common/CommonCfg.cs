﻿using System.Runtime.Serialization;

namespace commons
{
	[DataContract(Name = "commons", Namespace = "")]
	internal sealed class CommonCfg
	{
		[DataMember(Name = "host", Order = 0)]
		public string Host { get; set; }
		[DataMember(Name = "port", Order = 1)]
		public string Port { get; set; }
		[DataMember(Name = "user", Order = 2)]
		public User User { get; set; }
	}

	[DataContract(Name = "user", Namespace = "")]
	internal class User
	{
		[DataMember(Name = "minRank", Order = 0)]
		public int MinRank { get; set; }
		[DataMember(Name = "maxRank", Order = 1)]
		public int MaxRank { get; set; }
	}
}
