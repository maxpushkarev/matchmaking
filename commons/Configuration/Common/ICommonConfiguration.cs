﻿namespace commons
{
	public interface ICommonConfiguration
	{
		string Host { get; }
		string Port { get; }
		int MinUserRank { get; }
		int MaxUserRank { get; }
	}
}
