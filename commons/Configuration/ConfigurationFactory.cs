﻿using System;

namespace commons
{
	public static class ConfigurationFactory<T> where T : BaseConfiguration, new()
	{
		private static readonly object objLock = new object();
		private static Lazy<T> INSTANCE = new Lazy<T>(() => new T(), true);

		public static T Get() {
			return INSTANCE.Value;
		}
	}
}
