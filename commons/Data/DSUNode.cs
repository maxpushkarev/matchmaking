﻿namespace commons
{
	public abstract class DSUNode
	{
		protected DSUNode parent;
		private int rank;
		protected virtual int Rank { get { return rank; } }

		public DSUNode()
		{
		}

		public virtual DSUNode FindSet()
		{
			if (parent == null)
			{
				return this;
			}

			parent = parent.FindSet();
			return parent;
		}

		protected static void Swap<T>(ref T node1, ref T node2) where T : DSUNode
		{
			T tmp = node1;
			node1 = node2;
			node2 = tmp;
		}

		public static DSUNode operator +(DSUNode set1, DSUNode set2)
		{
			DSUNode source;
			DSUNode destination;
			Union<DSUNode>(set1, set2, out source, out destination);
			return destination;
		}

		protected static void Union<T>(T set1, T set2, out T source, out T destination) where T : DSUNode
		{
			T root1 = set1.FindSet() as T;
			T root2 = set2.FindSet() as T;

			if (ReferenceEquals(root1, root2))
			{
				source = destination = root1;
				return;
			}

			if (root1.Rank < root2.Rank)
			{
				Swap(ref root1, ref root2);
			}

			root2.parent = root1;

			if (root1.rank == root2.rank)
			{
				root1.rank++;
			}

			source = root2;
			destination = root1;
		}
	}

	public class DSUNode<T> : DSUNode
	{
		public readonly T value;
		public DSUNode(T value) : base()
		{
			this.value = value;
		}

		public override DSUNode FindSet()
		{
			return base.FindSet();
		}
	}
}
