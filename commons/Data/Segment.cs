﻿using System;

namespace commons
{
	public abstract class Segment<T> where T : struct, IComparable<T>
	{
		public bool Inserted { get; private set; }
		private event Action OnRemoveEvt;
		private event Action OnUpdateEvt;

		public void Init(Action onUpdate, Action onRemove)
		{
			if(Inserted)
			{
				throw new InvalidOperationException("Segment already exists in tree");
			}

			OnUpdateEvt += onUpdate;
			OnRemoveEvt += onRemove;
			Inserted = true;
		}

		public void Update()
		{
			if (!Inserted)
			{
				throw new InvalidOperationException("Segment not in tree");
			}

			OnUpdateEvt.Invoke();
		}

		public void Remove()
		{
			if (!Inserted)
			{
				throw new InvalidOperationException("Segment not in tree");
			}
			
			OnRemoveEvt.Invoke();
			OnRemoveEvt = null;
			OnUpdateEvt = null;
			Inserted = false;
		}

		public abstract void GetSegment(out T min, out T max);
	}
}
