﻿using System;
using System.Collections.Generic;

namespace commons
{
	public class SegmentTree<TSegment, TKey>
		where TKey : struct, IComparable<TKey>
		where TSegment : Segment<TKey>
	{
		private SegmentNode root;
		private Random rnd = new Random();

		public SegmentTree(TKey min, TKey max)
		{
			int diff = Convert.ToInt32((dynamic)max - min);

			if (diff <= 0)
			{
				throw new ArgumentException(string.Format("Cannot create segment tree with min = {0} and max = {1}", min, max));
			}

			double log2 = Math.Log(Convert.ToDouble(diff + 1), 2.0);
			double nearestPowerOfTwoDiff = Math.Pow(2.0, Math.Ceiling(log2));
			TKey correctedMax = Convert.ToInt32(Convert.ToDouble((dynamic)min) + nearestPowerOfTwoDiff) - 1;
			root = new SegmentNode(min, correctedMax);
			InitNode(root);
		}

		public void Put(TSegment segment)
		{
			SegmentNode node = FindSegmentNode(root, segment);
			if (node == null)
			{
				throw new ArgumentException("Cannot find node for segment");
			}

			var treeNode = node.segments.Add(segment);
			segment.Init(() => {
				segment.Remove();
				Put(segment);
			}, () => node.segments.Remove(treeNode));
		}

		public void DFSWithRandomSegments(Func<TKey, TKey, bool> condition, Action<TSegment> rndSegmentProcessor, int maxRndSegmentCount)
		{
			DFS(condition, n => {
				IEnumerable<TSegment> rndSegments = n.segments.GetRandomElements(maxRndSegmentCount);
				foreach(TSegment segment in rndSegments)
				{
					rndSegmentProcessor(segment);
				}
			});
		}

		private void DFS(Func<TKey, TKey, bool> condition, Action<SegmentNode> nodeProcessor)
		{
			DFS(root, condition, nodeProcessor);
		}

		private void DFS(SegmentNode node, Func<TKey, TKey, bool> condition, Action<SegmentNode> nodeProcessor)
		{
			if(node == null)
			{
				return;
			}

			if(!condition(node.min, node.max))
			{
				return;
			}

			nodeProcessor(node);

			DFS(node.left, condition, nodeProcessor);
			DFS(node.right, condition, nodeProcessor);
		}

		private SegmentNode FindSegmentNode(SegmentNode node, TSegment segment)
		{
			if(SegmentWhithinNodeRange(node.left, segment))
			{
				return FindSegmentNode(node.left, segment);
			}

			if (SegmentWhithinNodeRange(node.right, segment))
			{
				return FindSegmentNode(node.right, segment);
			}

			if(SegmentWhithinNodeRange(node, segment))
			{
				return node;
			}

			return null;
		}

		private bool SegmentWhithinNodeRange(SegmentNode node, TSegment segment)
		{
			if(node ==  null)
			{
				return false;
			}

			TKey min, max;
			segment.GetSegment(out min, out max);
			bool validMin = node.min.CompareTo(min) <= 0;
			bool validMax = node.max.CompareTo(max) >= 0;
			return validMax && validMin && min.CompareTo(max) <= 0;
		}

		private void InitNode(SegmentNode segmentNode)
		{

			int diff = Convert.ToInt32((dynamic)segmentNode.max - segmentNode.min);
			if (diff == 0)
			{
				return;
			}
			int halfDiff = diff / 2;

			TKey leftMin = segmentNode.min;
			TKey leftMax = (dynamic)segmentNode.min + halfDiff;
			SegmentNode left = new SegmentNode(leftMin, leftMax);

			TKey rightMin = (dynamic)segmentNode.max - halfDiff;
			TKey rightMax = segmentNode.max;
			SegmentNode right = new SegmentNode(rightMin, rightMax);

			segmentNode.left = left;
			segmentNode.right = right;

			InitNode(left);
			InitNode(right);
		}

		internal class SegmentNode
		{
			public readonly TKey min;
			public readonly TKey max;

			public readonly RandomAccessBinaryTree<TSegment> segments = new RandomAccessBinaryTree<TSegment>();

			public SegmentNode left;
			public SegmentNode right;

			internal SegmentNode(TKey min, TKey max)
			{
				this.min = min;
				this.max = max;
			}
		}
	}
}
