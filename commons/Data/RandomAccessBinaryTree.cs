﻿using System;
using System.Collections.Generic;

namespace commons
{
	internal sealed class RandomAccessBinaryTree<T>
	{
		private const double CURRENT_NODE_THRESHOLD = 0.3333333333;
		private const double LEFT_CHILD_THRESHOLD = 0.6666666666;
		private LinkedList<TreeNode> nodeList = new LinkedList<TreeNode>();
		private TreeNode root;
		private Random random = new Random();

		internal TreeNode Add(T data)
		{
			TreeNode newNode = new TreeNode(data);
			return Add(newNode);
		}

		private TreeNode Add(TreeNode node)
		{
			if(node.parent != null)
			{
				throw new ArgumentException("Node already added in tree");
			}

			if(node.llNode != null)
			{
				throw new ArgumentException("Node already added in tree");
			}

			node.llNode = nodeList.AddLast(node);
			root = root ?? node;

			if(root == node)
			{
				return node;
			}

			TreeNode parent = nodeList.First.Value;
			node.parent = parent;

			if(parent == node)
			{
				throw new InvalidOperationException("Cycle within tree!");
			}

			if (parent.left == null)
			{
				parent.left = node;
			} else if (parent.right == null)
			{
				parent.right = node;
			}
			else
			{
				throw new InvalidOperationException("Cannot add node in tree!");
			}

			if((parent.left != null) && (parent.right != null))
			{
				nodeList.RemoveFirst();
				parent.llNode = null;
			}

			return node;
		}

		internal void Remove(TreeNode node)
		{
			if(node.parent == null && !ReferenceEquals(node, root))
			{
				throw new InvalidOperationException("Inconsistent node");
			}

			if(IsLeaf(node))
			{
				RemoveLeaf(node);
				return;
			}

			TreeNode leaf = FindAnyLeaf(node);
			RemoveLeaf(leaf);
			ReplaceNodeWithLeaf(node, leaf);
		}

		private void ReplaceNodeWithLeaf(TreeNode old, TreeNode leaf)
		{
			leaf.parent = old.parent;
			if(old.parent != null)
			{
				if(old.parent.left == old)
				{
					old.parent.left = leaf;
				}
				else
				{
					old.parent.right = leaf;
				}
			}

			old.parent = null;

			leaf.left = old.left;
			leaf.right = old.right;

			bool needToAddInList = false;

			if(leaf.left != null)
			{
				leaf.left.parent = leaf;
			}
			else
			{
				needToAddInList |= true;
			}

			if (leaf.right != null)
			{
				leaf.right.parent = leaf;
			}
			else
			{
				needToAddInList |= true;
			}

			if(old.llNode != null)
			{
				nodeList.Remove(old.llNode);
				old.llNode = null;
			}

			if(needToAddInList)
			{
				leaf.llNode = nodeList.AddFirst(leaf);
			}

			if(old == root)
			{
				root = leaf;
			}
		}

		private TreeNode FindAnyLeaf(TreeNode node)
		{
			if(IsLeaf(node))
			{
				return node;
			}

			if(node.left != null)
			{
				return FindAnyLeaf(node.left);
			}

			if (node.right != null)
			{
				return FindAnyLeaf(node.right);
			}

			throw new InvalidOperationException("Impossible exception");
		}

		private bool IsLeaf(TreeNode node)
		{
			return node.left == null && node.right == null;
		}

		private void RemoveLeaf(TreeNode node)
		{
			if(node.llNode != null)
			{
				nodeList.Remove(node.llNode);
				node.llNode = null;
			}

			TreeNode parent = node.parent;
			if(parent == null)
			{
				if(node != root)
				{
					throw new InvalidOperationException("Non-root leaf without parent");
				}

				root = null;
				nodeList.Clear();
				return;
			}

			if(parent.left == node)
			{
				parent.left = null;
			}
			else
			{
				parent.right = null;
			}

			if(parent.llNode == null)
			{
				parent.llNode = nodeList.AddLast(parent);
			}
		}

		internal IEnumerable<T> GetRandomElements(int maxElementsCount)
		{
			LinkedList<T> collection = new LinkedList<T>();

			if(root == null)
			{
				return collection;
			}

			if(maxElementsCount == 0)
			{
				return collection;
			}

			TreeNode node = GetBaseNode(root);

			collection.AddLast(node.data);
			maxElementsCount--;

			AddRndDown(node.left, collection, ref maxElementsCount);
			AddRndDown(node.right, collection, ref maxElementsCount);
			AddRndUp(node.parent, node, collection, ref maxElementsCount);

			return collection;
		}

		private TreeNode GetBaseNode(TreeNode node)
		{
			if(node == null)
			{
				throw new ArgumentNullException("Node is null");
			}

			if ((node.left == null) && (node.right == null))
			{
				return node;
			}

			double rndVal = random.NextDouble();

			if(rndVal <= CURRENT_NODE_THRESHOLD)
			{
				return node;
			}

			if((rndVal <= LEFT_CHILD_THRESHOLD) && (node.left != null))
			{
				return GetBaseNode(node.left);
			}

			return GetBaseNode(node.right != null ? node.right : node.left);
		}

		private void AddRndUp(TreeNode node, TreeNode skipNode, LinkedList<T> collection, ref int counter)
		{
			if (!ValidateRndCollectionStep(node, counter))
			{
				return;
			}

			collection.AddLast(node.data);
			counter--;
			
			if(node.left != skipNode)
			{
				AddRndDown(node.left, collection, ref counter);
			}

			if (node.right != skipNode)
			{
				AddRndDown(node.right, collection, ref counter);
			}

			AddRndUp(node.parent, node, collection, ref counter);
		}

		private bool ValidateRndCollectionStep(TreeNode node, int counter)
		{
			if (node == null)
			{
				return false;
			}

			if (counter == 0)
			{
				return false;
			}

			if (counter < 0)
			{
				throw new InvalidOperationException("Rnd counter negative!");
			}

			return true;
		}

		private void AddRndDown(TreeNode node, LinkedList<T> collection, ref int counter)
		{
			if(!ValidateRndCollectionStep(node, counter))
			{
				return;
			}

			collection.AddLast(node.data);
			counter--;
			AddRndDown(node.left, collection, ref counter);
			AddRndDown(node.right, collection, ref counter);
		}

		internal class TreeNode
		{
			internal readonly T data;
			internal TreeNode left;
			internal TreeNode right;
			internal TreeNode parent;
			internal LinkedListNode<TreeNode> llNode;
			internal TreeNode(T data)
			{
				this.data = data;
			}
		}
	}
}