﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

[assembly: InternalsVisibleTo("commons.test")]
namespace commons
{
	public sealed class Commons
	{
		public static readonly int BUFFER_LENGTH = 256;

		public static void SendCommand<T>(NetworkStream networkStream) where T : ContextExecutableCommand, new()
		{
			SendCommand(networkStream, new T());
		}

		public static void SendCommand(NetworkStream networkStream, ContextExecutableCommand cmd)
		{
			SendCommand(networkStream, cmd, new BinaryFormatter());
		}

		public static async void SendCommand(NetworkStream networkStream, ContextExecutableCommand cmd, IFormatter formatter)
		{
			try
			{
				using (MemoryStream memoryStream = new MemoryStream())
				{
					formatter.Serialize(memoryStream, cmd);
					await networkStream.WriteAsync(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
				}
			}
			catch(IOException e)
			{
				if (!(e.InnerException is SocketException))
				{
					throw;
				}
			}
		}

		public static ContextExecutableCommand DeserializeCommand(byte[] buffer, int count, IFormatter formatter)
		{
			using (MemoryStream readMemoryStream = new MemoryStream(buffer, 0, count, false))
			{
				return formatter.Deserialize(readMemoryStream) as ContextExecutableCommand;
			}
		}

		public static int RndRange(Random random, int min, int max)
		{
			return Convert.ToInt32(Math.Floor(random.NextDouble() * (max - min))) + min;
		}
	}
}
