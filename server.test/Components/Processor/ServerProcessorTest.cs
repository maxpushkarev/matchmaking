﻿using commons;
using lobby;
using matchmaking;
using NUnit.Framework;
using server.core;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;

namespace server.test
{
	[TestFixture]
	internal sealed class ServerProcessorTest
	{
		private static TestServerComponent TEST_COMPONENT;
		private TestNetworkConfiguration testNetworkCfg;
		private TestServerConfiguration testServerConfiguration;
		private ServerNetwork serverNetwork;
		private ServerCommandProcessor serverCommandProcessor;
		private const int LOBBIES_COUNT = 5;
		private const int SESSIONS_PER_LOBBY = 3;

		[SetUp]
		public void SetUp()
		{
			testNetworkCfg = ConfigurationFactory<TestNetworkConfiguration>.Get();
			CommonInjectorActivator.Bind<ICommonConfiguration>(testNetworkCfg);
			testServerConfiguration = ConfigurationFactory<TestServerConfiguration>.Get();
			CommonInjectorActivator.Bind<IServerConfiguration>(
				testServerConfiguration
			);

			serverCommandProcessor = new ServerCommandProcessor();
			CommonInjectorActivator.Bind<ServerCommandConsumerCLI>(serverCommandProcessor);

			serverNetwork = new ServerNetwork();
			CommonInjectorActivator.Bind<ServerNetwork>(serverNetwork);
			CommonInjectorActivator.Get<ServerNetwork>();
		}

		[TearDown]
		public void TearDown()
		{
			TEST_COMPONENT.Dispose();
			serverNetwork.Dispose();
			serverCommandProcessor.Dispose();
		}

		[Test]
		public void ShouldBalanceSessionsWithinLobbyServers()
		{
			TEST_COMPONENT = new TestServerComponent((t, cmd) => {
				int count;
				if (t.lobbies2SessionsCount.TryGetValue(cmd.lobbyName, out count))
				{
					count++;
					t.lobbies2SessionsCount[cmd.lobbyName] = count;
				}
				else
				{
					t.lobbies2SessionsCount[cmd.lobbyName] = 1;
				}

				t.connectedCounter++;
				if (t.connectedCounter == LOBBIES_COUNT * SESSIONS_PER_LOBBY)
				{
					t.Stop();
				}
			}, null);

			TEST_COMPONENT.Start();
			serverCommandProcessor.Start();
			serverNetwork.Start();

			TcpClient[] clients = new TcpClient[LOBBIES_COUNT * SESSIONS_PER_LOBBY];
			for (int i = 1; i <= LOBBIES_COUNT * SESSIONS_PER_LOBBY; i++)
			{
				clients[i - 1] = CreateTestClient();
			}

			TEST_COMPONENT.Wait();
			Array.ForEach(clients, c => c.Dispose());

			Assert.AreEqual(LOBBIES_COUNT, TEST_COMPONENT.lobbies2SessionsCount.Values.Count);
			foreach(int val in TEST_COMPONENT.lobbies2SessionsCount.Values)
			{
				Assert.AreEqual(SESSIONS_PER_LOBBY, val);
			}
		}


		[Test]
		public void ShouldSupportBalanceWhenDisconnect()
		{
			string lobbyName = null;
			AutoResetEvent firstConnectionEvt = new AutoResetEvent(false);
			AutoResetEvent firstDisconnectEvt = new AutoResetEvent(false);
			bool firstDisconnect = true;
			TEST_COMPONENT = new TestServerComponent((t, cmd) => {

				int count;
				if (t.lobbies2SessionsCount.TryGetValue(cmd.lobbyName, out count))
				{
					count++;
					t.lobbies2SessionsCount[cmd.lobbyName] = count;
				}
				else
				{
					t.lobbies2SessionsCount[cmd.lobbyName] = 1;
				}

				t.connectedCounter++;
				if (t.connectedCounter == 1)
				{
					lobbyName = cmd.lobbyName;
					firstConnectionEvt.Set();
					return;
				}

				if (t.connectedCounter == LOBBIES_COUNT + 1)
				{
					t.Stop();
				}
			}, (t) => { 
				if(firstDisconnect)
				{
					firstDisconnectEvt.Set();
					firstDisconnect = false;
				}
			});

			using (firstDisconnectEvt)
			using (firstConnectionEvt)
			{
				TEST_COMPONENT.Start();
				serverCommandProcessor.Start();
				serverNetwork.Start();

				TcpClient firstClient = CreateTestClient();
				firstConnectionEvt.WaitOne();

				TcpClient[] clients = new TcpClient[LOBBIES_COUNT - 1];
				for (int i = 1; i < LOBBIES_COUNT; i++)
				{
					clients[i - 1] = CreateTestClient();
				}
				firstClient.Dispose();
				firstDisconnectEvt.WaitOne();
				TcpClient another = CreateTestClient();
				another.Dispose();

				Array.ForEach(clients, c => c.Dispose());

				TEST_COMPONENT.Wait();
			}

			Assert.AreEqual(2, TEST_COMPONENT.lobbies2SessionsCount[lobbyName]);
		}


		private TcpClient CreateTestClient()
		{
			return new TcpClient(testNetworkCfg.Host, int.Parse(testNetworkCfg.Port));
		}

		private class TestServerConfiguration : BaseConfiguration, IServerConfiguration
		{
			public int LobbiesCount
			{
				get { return LOBBIES_COUNT; }
			}

			public Type LobbyType
			{
				get { return typeof(TestLobbyServerComponent); }
			}

			public object[] LobbyArguments
			{
				get { return new object[] { TEST_COMPONENT }; }
			}

			public Type MatchmakingType
			{
				get { return typeof(MatchmakingStub); }
			}
		}

		private class TestServerComponent : ServerCommandConsumer
		{
			public Dictionary<string, int> lobbies2SessionsCount = new Dictionary<string, int>();
			public int connectedCounter;
			private Action<TestServerComponent, TestConnectSessionCmd> onConnect;
			private Action<TestServerComponent> onDisconnect;

			public TestServerComponent(
				Action<TestServerComponent, TestConnectSessionCmd> onConnect,
				Action<TestServerComponent> onDisconnect
				) : base("TEST_ONLY_COMPONENT")
			{
				this.onConnect = onConnect;
				this.onDisconnect = onDisconnect;
			}

			[ExecuteCommandHandler]
			private void OnConnected(TestConnectSessionCmd cmd)
			{
				if (onConnect == null)
				{
					return;
				}
				onConnect(this, cmd);
			}

			[ExecuteCommandHandler]
			private void OnDisconnect(TestDisconnectSessionCmd cmd)
			{
				if (onDisconnect == null)
				{
					return;
				}
				onDisconnect(this);
			}
		}

		[Serializable]
		private class TestConnectSessionCmd : BaseExecutableCommand
		{
			public readonly string lobbyName;
			public TestConnectSessionCmd(string lobbyName)
			{
				this.lobbyName = lobbyName;
			}
		}

		[Serializable]
		private class TestDisconnectSessionCmd : BaseExecutableCommand
		{
		}

		private class MatchmakingCfgStub : BaseConfiguration, IMatchmakingConfiguration
		{
			public Func<int, int, long> WaitTimeFunc { get; private set; }
			public Func<int, long, long> UserWeightFunc { get; private set; }
			public int UsersInMatchCount { get; private set; }
			public int ImproveUserWeightInterval { get; private set; }
			public int MaxRndSetSelectCount { get; }

			public MatchmakingCfgStub()
			{
				UsersInMatchCount = 2;
			}
		}

		private class MatchmakingStub : MatchmakingServerComponent<MatchmakingCfgStub, TestNetworkConfiguration>
		{
			public MatchmakingStub(string name, ServerCommandConsumer router) : base(name, router)
			{
			}

			protected override void ExecuteEnterMatchmaking(ContextConnectedExecutableCommand<User, CrossDomainCommandContext> cmd)
			{
			}
		}



		private class TestLobbyServerComponent : BaseLobbyServerComponent
		{
			private TestServerComponent testComponent;

			public TestLobbyServerComponent(string name, ServerCommandConsumer router, BaseMatchmakingServerComponent matchmaking, TestServerComponent testComponent) : base(name, router, matchmaking)
			{
				this.testComponent = testComponent;
			}

			protected override void ExecuteSessionConnectedCommand(ContextConnectedExecutableCommand<Session, CrossDomainCommandContext> cmd)
			{
				CrossDomainCommandContext context = cmd.contextProxy;
				testComponent.PutCommand(new TestConnectSessionCmd(Name));

				RegisterContextExecutableHandler<ContextDisconnectedExecutableCommand>(
					context, 
					disconnectedCmd => {
						testComponent.PutCommand(new TestDisconnectSessionCmd());
					}
				);
			}
		}
	}
}
