﻿using commons;
using NUnit.Framework;
using server.core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Threading.Tasks;

namespace server.test
{
	[TestFixture]
	internal sealed class ServerNetworkTest
	{
		private TestNetworkConfiguration testConfiguration;

		[SetUp]
		public void SetUp()
		{
			testConfiguration = ConfigurationFactory<TestNetworkConfiguration>.Get();
			CommonInjectorActivator.Bind<ICommonConfiguration>(testConfiguration);
		}

		[Test]
		public void ClientServerPingPongTest()
		{
			PingPongConsumer pingPongConsumer = new PingPongConsumer();

			using (pingPongConsumer)
			{
				CommonInjectorActivator.Bind<ServerCommandConsumerCLI>(pingPongConsumer);
				ServerNetwork serverNetwork = new ServerNetwork();
				using (serverNetwork)
				{
					CommonInjectorActivator.Bind<ServerNetwork>(serverNetwork);
					CommonInjectorActivator.Get<ServerNetwork>();

					pingPongConsumer.serverNetwork = serverNetwork;
					pingPongConsumer.Start();
					serverNetwork.Start();

					using (TcpClient client = new TcpClient(testConfiguration.Host, int.Parse(testConfiguration.Port)))
					using (NetworkStream ns = client.GetStream())
					{
						Commons.SendCommand<PingCommand>(ns);
						byte[] buf = new byte[Commons.BUFFER_LENGTH];
						int bytesRead = ns.Read(buf, 0, Commons.BUFFER_LENGTH);
						var pong = Commons.DeserializeCommand(buf, bytesRead, new BinaryFormatter()) as PongCommand;
						if(pong == null)
						{
							throw null;
						}
					}
				}
			}
		}

		[Test]
		public void ConnectAndDisconnetCommandsShouldBeInCorrectOrder()
		{
			TestServerCommandConsumer consumer = new TestServerCommandConsumer();

			using (consumer)
			{
				CommonInjectorActivator.Bind<ServerCommandConsumerCLI>(consumer);
				ServerNetwork serverNetwork = new ServerNetwork();
				using (serverNetwork)
				{
					CommonInjectorActivator.Bind<ServerNetwork>(serverNetwork);
					CommonInjectorActivator.Get<ServerNetwork>();
					
					consumer.Start();
					serverNetwork.Start();
					using (Task client1 = CreateTestClient())
					using (Task client2 = CreateTestClient())
					{
						Task.WaitAll(client1, client2);
						consumer.Wait();
					}
				}
			}

			Assert.That(
				consumer.sessionCommandExecutionData.Values.All(i => i==2) && consumer.sessionCommandExecutionData.Values.Count == 2
			);
		}

		private Task CreateTestClient()
		{
			int connectionTimeoutMs = GenerateRandomTimeoutMs(100, 500);
			int disconnectTimeoutMs = GenerateRandomTimeoutMs(150, 300);
			return Task.Factory.StartNew(() => {

				Thread.Sleep(connectionTimeoutMs);

				using (TcpClient client = new TcpClient(testConfiguration.Host, int.Parse(testConfiguration.Port)))
				{
					Thread.Sleep(disconnectTimeoutMs);
				}

			});
		}

		private int GenerateRandomTimeoutMs(int min, int max)
		{
			return Convert.ToInt32(new Random().NextDouble() * (max - min) + min);
		}

		private class PingPongConsumer : ServerCommandConsumerCLI
		{
			public ServerNetwork serverNetwork;
			public PingPongConsumer() : base(string.Empty)
			{
			}

			[ExecuteCommandHandler]
			private void OnConnect(ContextConnectedExecutableCommand<Session> cmd)
			{
				Session session = cmd.context;
				RegisterContextExecutableHandler<PingCommand>(session, pingCommand => {
					serverNetwork.PutCommand(session, new Server2ClientNetworkCommand(new PongCommand()));
				});

				RegisterContextExecutableHandler<ContextDisconnectedExecutableCommand>(session, dis =>
				{
				});
			}
		}

		[Serializable]
		private class PingCommand : ContextExecutableCommand
		{
		}

		[Serializable]
		private class PongCommand : ContextExecutableCommand
		{
		}

		private class TestServerCommandConsumer : ServerCommandConsumerCLI
		{
			private int disconnectCount;
			public Dictionary<long, byte> sessionCommandExecutionData = new Dictionary<long, byte>();

			public TestServerCommandConsumer() : base(string.Empty)
			{
			}

			[ExecuteCommandHandler]
			private void OnConnect(ContextConnectedExecutableCommand<Session> cmd)
			{
				Session session = cmd.context;
				RegisterContextExecutableHandler<ContextDisconnectedExecutableCommand>(
					session,
					command => {
						sessionCommandExecutionData[session.sessionId] <<= 1;
						disconnectCount++;
						if (disconnectCount == 2)
						{
							Stop();
						}
					}
				);
				sessionCommandExecutionData.Add(session.sessionId, 1);
			}
		}
	}
}
