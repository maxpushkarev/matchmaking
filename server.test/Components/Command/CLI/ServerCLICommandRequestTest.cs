﻿using NUnit.Framework;
using System;

namespace server.test
{
	[TestFixture]
	internal sealed class ServerCLICommandRequestTest
	{
		[Test]
		public void ShouldNotParseNullCLIString() {
			ServerCLICommandRequest entry;
			bool res = ServerCLICommandRequest.TryParse(null, out entry);
			Assert.IsFalse(res);
		}

		[Test]
		public void ShouldNotParseEmptyCLIString()
		{
			ServerCLICommandRequest entry;
			bool res = ServerCLICommandRequest.TryParse(string.Empty, out entry);
			Assert.IsFalse(res);
		}

		[Test]
		public void ShouldParseMethodOnlyCLIString()
		{
			ServerCLICommandRequest entry;
			string method = "method";
			bool res = ServerCLICommandRequest.TryParse(method, out entry);
			Assert.IsTrue(res);
			Assert.AreEqual(method, entry.method);
		}

		[Test]
		public void ShouldParseMethodOnlyWithSpacesCLIString()
		{
			ServerCLICommandRequest entry;
			string method = "	method_123     ";
			string expected = "method_123";
			bool res = ServerCLICommandRequest.TryParse(method, out entry);
			Assert.IsTrue(res);
			Assert.AreEqual(expected, entry.method);
		}

		[Test]
		public void ShouldParseMethodWithArgsCLIString()
		{
			ServerCLICommandRequest entry;
			string method = "	method_123   uy2t oo77io	_62ll ii ";
			bool res = ServerCLICommandRequest.TryParse(method, out entry);
			Assert.IsTrue(res);
			Assert.AreEqual("method_123", entry.method);

			int i = 0;
			Array.ForEach(new string[] {
				"uy2t",
				"oo77io",
				"_62ll",
				"ii"
			}, s => {
				Assert.AreEqual(s, entry.args[i++]);
			});
		}
	}
}
