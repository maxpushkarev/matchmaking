﻿using NUnit.Framework;
using server.core;
using System.Collections.Generic;
using System.Linq;

namespace server.test
{
	[TestFixture]
	internal sealed class ServerCLICommandTest
	{
		private const string DESCRIPTION = "description";

		[Test]
		public void ShouldThrowCLICollisionException()
		{
			Assert.Throws<ServerCLICommandException>(() => { using (new TestServerCommandCollision()) { } });
		}

		[Test]
		public void ShouldThrowCLIInvalidSignatureExceptionWhenStringReturnType()
		{
			Assert.Throws<ServerCLICommandException>(() => { using (new TestServerCommandString()) { } });
		}

		[Test]
		public void ShouldThrowCLIInvalidSignatureExceptionWhenMethodArgNotString()
		{
			Assert.Throws<ServerCLICommandException>(() => { using (new TestServerCommandLong()) { } });
		}

		[Test]
		public void ShouldCreateOneCLICommand()
		{
			using (TestServerCommandValid testServerCommandValid = new TestServerCommandValid())
			{
				Assert.AreEqual(1, testServerCommandValid.GetCmds().Count);
				Assert.AreEqual(DESCRIPTION, testServerCommandValid.GetCmds().First().Value.serverCLICommandAttribute.description);
			}
		}

		[Test]
		public void ShouldCreateTwoCLICommandsWhenTwoMethodsWithTheSameName()
		{
			using (TestServerCommandTwoMethods testServerCommand = new TestServerCommandTwoMethods())
			{
				Assert.AreEqual(2, testServerCommand.GetCmds().Count);
			}
		}

		[Test]
		public void ShouldCreateTwoCLICommandsWhenExcplicitInterfaceMethod()
		{
			using (TestServerCommandExplicitInterfaceImpl testServerCommand = new TestServerCommandExplicitInterfaceImpl())
			{
				Assert.AreEqual(2, testServerCommand.GetCmds().Count);
			}
		}

		private class TestServerCommandWithExecuteHandlerAttribute : ServerCommandConsumerCLI
		{
			[ExecuteCommandHandler]
			[ServerCLICommand(DESCRIPTION)]
			private void TestCmd()
			{
			}

			public TestServerCommandWithExecuteHandlerAttribute() : base(string.Empty)
			{
			}
		}

		private class BaseTestServerCommandConsumerCLI : ServerCommandConsumerCLI
		{
			public BaseTestServerCommandConsumerCLI() : base(string.Empty)
			{
			}
		}

		private class TestServerCommandString : BaseTestServerCommandConsumerCLI
		{
			[ServerCLICommand(DESCRIPTION)]
			private string TestCmd() {
				return string.Empty;
			}
		}

		private class TestServerCommandCollision : BaseTestServerCommandConsumerCLI
		{
			[ServerCLICommand(DESCRIPTION)]
			private void TestCmd()
			{
			}

			[ServerCLICommand(DESCRIPTION)]
			private void Testcmd()
			{
			}
		}

		private class TestServerCommandLong : BaseTestServerCommandConsumerCLI
		{
			[ServerCLICommand(DESCRIPTION)]
			private void TestCmd(string pty, long e)
			{
			}
		}

		private class TestServerCommandValid : BaseTestServerCommandConsumerCLI
		{
			[ServerCLICommand(DESCRIPTION)]
			private void TestCmd(string pty, string op)
			{
			}

			private void TestCmd(int i, decimal d)
			{
			}

			public Dictionary<string, ServerCLICommandInfo> GetCmds() {
				return CLICommands;
			}
		}

		private class TestServerCommandTwoMethods : BaseTestServerCommandConsumerCLI
		{
			[ServerCLICommand(DESCRIPTION)]
			private void TestCmd(string pty, string op)
			{
			}

			[ServerCLICommand(DESCRIPTION)]
			private void TestCmd(string pty)
			{
			}

			public Dictionary<string, ServerCLICommandInfo> GetCmds()
			{
				return CLICommands;
			}
		}

		private class TestServerCommandExplicitInterfaceImpl : BaseTestServerCommandConsumerCLI, TestInterface
		{
			[ServerCLICommand(DESCRIPTION)]
			public void TestCmd()
			{
			}

			[ServerCLICommand(DESCRIPTION)]
			void TestInterface.TestCmd()
			{
			}

			public Dictionary<string, ServerCLICommandInfo> GetCmds()
			{
				return CLICommands;
			}
		}

		private interface TestInterface 
		{
			void TestCmd();
		}
	}
}
