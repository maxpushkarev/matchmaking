﻿using commons;

namespace server.test
{
	internal class TestNetworkConfiguration : BaseConfiguration, ICommonConfiguration
	{
		public string Host => "127.0.0.1";
		public string Port => "6677";
		public int MinUserRank { get { return 0; } }
		public int MaxUserRank { get { return 123; } }
	}
}
