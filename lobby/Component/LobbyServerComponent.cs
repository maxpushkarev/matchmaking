﻿using commons;
using matchmaking;
using server.core;
using System;
using System.Threading.Tasks;

namespace lobby
{
	public class LobbyServerComponent : BaseLobbyServerComponent
	{
		private LobbyConfiguration lobbyConfiguration;
		private CommonConfiguration commonConfiguration;
		private Random random;

		public LobbyServerComponent(string name, ServerCommandConsumer router, BaseMatchmakingServerComponent matchmaking) : base(name, router, matchmaking)
		{
			lobbyConfiguration = ConfigurationFactory<LobbyConfiguration>.Get();
			commonConfiguration = ConfigurationFactory<CommonConfiguration>.Get();
			random = new Random();
		}

		protected override void ExecuteSessionConnectedCommand(ContextConnectedExecutableCommand<Session, CrossDomainCommandContext> cmd)
		{
			Session routerSession = cmd.context;
			CrossDomainCommandContext lobbyContext = cmd.contextProxy;

			RegisterContextExecutableHandler<ContextDisconnectedExecutableCommand>(
				lobbyContext,
				disconnectedCommand => {
					UnregisterContext(lobbyContext);
				}
			);

			ExecuteAuth(routerSession, lobbyContext);
		}

		private async void ExecuteAuth(Session session, CrossDomainCommandContext lobbyContext)
		{
			RegisterContextExecutableHandler<UserLoadedExecutableCommand>(lobbyContext, cmd =>
			{
				User loadedUser = ((UserLoadedExecutableCommand)cmd).user;

				RegisterContextExecutableHandler<EnterBattleExecutableCommand>(lobbyContext, enterCommand =>
				{
					CrossDomainCommandContext mm2lobbyProxy = CreateTransparentProxyObject<CrossDomainCommandContext>(matchmaking.Domain);
					RegisterContextExecutableHandler<ContextDisconnectedExecutableCommand>(lobbyContext, d =>
					{
						matchmaking.PutCommand(mm2lobbyProxy, new ContextDisconnectedExecutableCommand());
					});
					matchmaking.PutCommand(new ContextConnectedExecutableCommand<User, CrossDomainCommandContext>(loadedUser, mm2lobbyProxy));
				});

				router.PutCommand(session, new Server2ClientNetworkCommand(new UserAuthorizedExecutableCommand()));
			});

			//emulate auth
			await Task.Delay(GetRandomAuthDelay());
			User user = new User(session.sessionId, GetRandomUserRank());
			PutCommand(lobbyContext, new UserLoadedExecutableCommand(user));
		}

		private int GetRandomMinMax(int min, int max)
		{
			return Commons.RndRange(random, min, max);
		}

		private int GetRandomAuthDelay()
		{
			return GetRandomMinMax(lobbyConfiguration.MinAuthDelay, lobbyConfiguration.MaxAuthDelay);
		}

		private int GetRandomUserRank()
		{
			return GetRandomMinMax(commonConfiguration.MinUserRank, commonConfiguration.MaxUserRank+1);
		}

		private class UserLoadedExecutableCommand : ContextExecutableCommand
		{
			public readonly User user;
			public UserLoadedExecutableCommand(User user)
			{
				this.user = user;
			}
		}
	}
}
