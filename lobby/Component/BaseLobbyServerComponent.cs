﻿using matchmaking;
using server.core;

namespace lobby
{
	public abstract class BaseLobbyServerComponent : ServerCommandConsumer
	{
		protected BaseMatchmakingServerComponent matchmaking;
		protected ServerCommandConsumer router;

		public BaseLobbyServerComponent(string name, ServerCommandConsumer router, BaseMatchmakingServerComponent matchmaking) : base(name)
		{
			this.matchmaking = matchmaking;
			this.router = router;
		}

		[ExecuteCommandHandler]
		protected void OnSessionConnected(ContextConnectedExecutableCommand<Session, CrossDomainCommandContext> cmd)
		{
			ExecuteSessionConnectedCommand(cmd);
		}

		protected abstract void ExecuteSessionConnectedCommand(ContextConnectedExecutableCommand<Session, CrossDomainCommandContext> cmd);
	}
}
