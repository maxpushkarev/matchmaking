﻿using commons;
using System.Reflection;
using System.Resources;

namespace lobby
{
	internal sealed class LobbyConfiguration : BaseConfiguration
	{
		private ResourceManager resourceManager;
		public int MinAuthDelay { get; private set; }
		public int MaxAuthDelay { get; private set; }


		public LobbyConfiguration()
		{
			resourceManager = new ResourceManager("lobby.Lobby", Assembly.GetAssembly(typeof(LobbyConfiguration)));
			MinAuthDelay = int.Parse(resourceManager.GetString("MinAuthorizationDelay"));
			MaxAuthDelay = int.Parse(resourceManager.GetString("MaxAuthorizationDelay"));
		}
	}
}
