﻿using C5;
using commons;
using lobby;
using matchmaking;
using Ninject;
using server.core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace server
{
	internal sealed class ServerCommandProcessor : ServerCommandConsumerCLI
	{
		//TODO: replace with single-end priority queue
		private IPriorityQueue<LoadedLobby> lobbies;
		private static string CONSOLE_LINE_SEPARATOR = string.Format("..........................................{0}", Environment.NewLine);

		[Inject]
		public IServerConfiguration ServerConfiguration { get; set; }
		[Inject]
		public ServerNetwork ServerNetwork { get; set; }

		public ServerCommandProcessor() : base("servercommandprocessor")
		{
		}

		[ExecuteCommandHandler]
		private void ExecuteCLICommand(ServerCLIExecutableCommand command)
		{
			command.serverCLICommandInfo.method.Invoke(this, command.serverCLICommandRequest.args);
		}

		[ExecuteCommandHandler]
		private void ExecuteSessionConnectedCommand(ContextConnectedExecutableCommand<Session> sessionConnectedCmd)
		{
			Session session = sessionConnectedCmd.context;

			IPriorityQueueHandle<LoadedLobby> minLoadedLobbyHandle;
			LoadedLobby minLoadedLobby = lobbies.FindMin(out minLoadedLobbyHandle);
			minLoadedLobby.IncrementSessions();
			lobbies.Replace(minLoadedLobbyHandle, minLoadedLobby);

			CrossDomainCommandContext lobby2ProcessorProxy = CreateTransparentProxyObject<CrossDomainCommandContext>(minLoadedLobby.Lobby.Domain);

			RegisterContextExecutableHandler<EnterBattleExecutableCommand>(session, cmd =>
			{
				minLoadedLobby.Lobby.PutCommand(lobby2ProcessorProxy, cmd);
			});

			RegisterContextExecutableHandler<ContextDisconnectedExecutableCommand>(session,
			sessionDisconnectedCmd =>
			{
				minLoadedLobby.Lobby.PutCommand(lobby2ProcessorProxy, sessionDisconnectedCmd);
				UnregisterContext(session);
				minLoadedLobby.DecrementSessions();
				lobbies.Replace(minLoadedLobbyHandle, minLoadedLobby);
			});

			RegisterContextExecutableHandler<Server2ClientNetworkCommand>(session,
			s2c =>
			{
				ServerNetwork.PutCommand(session, s2c);
			});

			minLoadedLobby.Lobby.PutCommand(new ContextConnectedExecutableCommand<Session, CrossDomainCommandContext>(session, lobby2ProcessorProxy));
		}


		[ServerCLICommand("Show lobby balance")]
		private async void LobbyBalance()
		{
			string[] lobbies2Strings = lobbies.Select(
					ll => string.Format("{0}_sessions: {1}{2}", ll.Lobby.Name, ll.SessionsCount, Environment.NewLine)
			).ToArray();

			using (Task task = Task.Factory.StartNew(() =>
				 Console.Write(
					 string.Join(
					 CONSOLE_LINE_SEPARATOR,
					 lobbies2Strings
				 )
			))) { await task; }
		}

		[ServerCLICommand("Shutdown server")]
		private void Shutdown()
		{
			Stop();
		}

		[ServerCLICommand("Shows list of all commands")]
		private async void Help()
		{
			using (Task task = Task.Factory.StartNew(() =>
				 Console.Write(
					 string.Join(
					 CONSOLE_LINE_SEPARATOR,
					 CLICommands
					 .Select(
						 p => string.Format("{0} | {1}{2}", p.Value, p.Value.serverCLICommandAttribute.description, Environment.NewLine))
					 )
				 )
			)) { await task; }
		}

		[ServerCLICommand("Run forced GC for all generations")]
		private void GC()
		{
			long memoryBeforeGC = System.GC.GetTotalMemory(true);
			System.GC.Collect();
			long memoryAfterGC = System.GC.GetTotalMemory(true);
			Console.WriteLine("Memory before GC: {0} ; after GC:{1}", memoryBeforeGC, memoryAfterGC);
		}

		public override void Start()
		{
			InitLogicalComponents();
			base.Start();
		}

		private void InitLogicalComponents()
		{
			AppDomainSetup setup = new AppDomainSetup()
			{
				ApplicationBase = Thread.GetDomain().SetupInformation.ApplicationBase
			};


			BaseMatchmakingServerComponent matchmakingServerComponent = InitLogicalComponent(
				ServerConfiguration.MatchmakingType, 
				"matchmaking", 
				setup, 
				this
			) as BaseMatchmakingServerComponent;

			matchmakingServerComponent.Start();

			int lobbyCounter = 0;

			//has no support for MemoryType Strict or Safe
			lobbies = new IntervalHeap<LoadedLobby>(ServerConfiguration.LobbiesCount, MemoryType.Normal);
			object[] lobbyCstrParams = new object[ServerConfiguration.LobbyArguments.Length + 2];
			lobbyCstrParams[0] = this;
			lobbyCstrParams[1] = matchmakingServerComponent;
			Array.Copy(ServerConfiguration.LobbyArguments, 0, lobbyCstrParams, 2, ServerConfiguration.LobbyArguments.Length);

			IEnumerable<LoadedLobby> loadedLobbies = Enumerable.Repeat<Func<LoadedLobby>>(() => {
					BaseLobbyServerComponent lobby = InitLogicalComponent(
						ServerConfiguration.LobbyType, 
						string.Format("lobby_{0}", ++lobbyCounter), 
						setup,
						lobbyCstrParams
					) as BaseLobbyServerComponent;
					LoadedLobby loadedLobby = new LoadedLobby(lobby);
					loadedLobby.Lobby.Start();
					return loadedLobby;
				},
				ServerConfiguration.LobbiesCount
			).Select(f => f());

			foreach (LoadedLobby ll in loadedLobbies)
			{
				IPriorityQueueHandle<LoadedLobby> handle = null;
				lobbies.Add(ref handle, ll);
			}
		}


		private ServerCommandConsumer InitLogicalComponent(Type componentType, string domainName, AppDomainSetup setup, params object[] cstrArgs)
		{
			//simulate isolation
			AppDomain serverComponentDomain = AppDomain.CreateDomain(domainName, null, setup);
			object[] combinesCstrArgs = new object[cstrArgs.Length + 1];
			combinesCstrArgs[0] = domainName;
			Array.Copy(cstrArgs, 0, combinesCstrArgs, 1, cstrArgs.Length);

			return CreateTransparentProxyObject<ServerCommandConsumer>(
				serverComponentDomain, componentType, combinesCstrArgs
			);
		}
	}
}
