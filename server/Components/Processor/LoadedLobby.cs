﻿using lobby;
using System;
using System.Threading;

namespace server
{
	internal sealed class LoadedLobby : IComparable<LoadedLobby>
	{
		private long sessionsCount;
		
		//reading by threadpool
		public long SessionsCount { 
			get { return Interlocked.Read(ref sessionsCount); }
		}

		public BaseLobbyServerComponent Lobby { get; private set; }

		public LoadedLobby(BaseLobbyServerComponent lobbyServerComponent)
		{
			this.Lobby = lobbyServerComponent;
		}

		//writing by processor thread
		public void IncrementSessions()
		{
			Interlocked.Increment(ref sessionsCount);
		}

		public void DecrementSessions()
		{
			Interlocked.Decrement(ref sessionsCount);
		}

		public int CompareTo(LoadedLobby other)
		{
			//reading by processor thread
			return sessionsCount.CompareTo(other.sessionsCount);
		}
	}
}
