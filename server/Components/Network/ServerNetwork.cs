﻿using commons;
using Ninject;
using server.core;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Threading.Tasks;

namespace server
{
	internal sealed class ServerNetwork : ServerCommandConsumer
	{
		private TcpListener listener;
		private long sessionCounter;
		private bool disposed = false;
		private BinaryFormatter formatter = new BinaryFormatter();

		[Inject]
		public ICommonConfiguration CommonConfiguration { get; set; }
		[Inject]
		public ServerCommandConsumerCLI ServerCommandConsumerCLI { get; set; }

		public ServerNetwork() : base("servernetwork")
		{
		}

		public override void Start()
		{
			base.Start();

			listener = new TcpListener(
				IPAddress.Parse(CommonConfiguration.Host),
				Int32.Parse(CommonConfiguration.Port)

			);
			listener.Start();

			Task.Factory.StartNew(async () => {
				while (true)
				{
					TcpClient client = await listener.AcceptTcpClientAsync();
					AcceptClient(client);
				}
			});
		}



		private async void AcceptClient(TcpClient client)
		{
			Interlocked.Increment(ref sessionCounter);
			Session session = new Session(sessionCounter);

			using (client)
			using (NetworkStream networkStream = client.GetStream())
			{
				try
				{
					RegisterContextExecutableHandler<Server2ClientNetworkCommand>(session, cmd =>
					{
						Commons.SendCommand(networkStream, ((Server2ClientNetworkCommand)cmd).innerCmd, formatter);
					});

					ServerCommandConsumerCLI.PutCommand(new ContextConnectedExecutableCommand<Session>(session));

					byte[] buffer = new byte[Commons.BUFFER_LENGTH];
					while (true)
					{
						int bytesRead = await networkStream.ReadAsync(buffer, 0, Commons.BUFFER_LENGTH);
						if(bytesRead == 0)
						{
							break;
						}

						ContextExecutableCommand clientCmd = Commons.DeserializeCommand(buffer, bytesRead, formatter);
						ServerCommandConsumerCLI.PutCommand(session, clientCmd);
					}

				}
				catch (IOException e)
				{
					if (!(e.InnerException is SocketException))
					{
						throw;
					}
				}
				finally
				{
					UnregisterContext(session);
					ServerCommandConsumerCLI.PutCommand(session, new ContextDisconnectedExecutableCommand());
				}
			}
		}


		protected override void Dispose(bool disposing)
		{
			if (disposed)
			{
				return;
			}

			if (disposing)
			{
				if(listener != null)
				{
					listener.Stop();
				}
			}

			disposed = true;
			base.Dispose(disposing);
		}
	}
}
