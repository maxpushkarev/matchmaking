﻿using System;

namespace server
{
	[AttributeUsage(AttributeTargets.Method)]
	internal sealed class ServerCLICommandAttribute : Attribute
	{
		public readonly string description;

		public ServerCLICommandAttribute(string description)
		{
			this.description = description;
		}
	}
}