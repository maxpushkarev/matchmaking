﻿using commons;

namespace server
{
	internal sealed class ServerCLIExecutableCommand : BaseExecutableCommand
	{
		public readonly ServerCLICommandInfo serverCLICommandInfo;
		public readonly ServerCLICommandRequest serverCLICommandRequest;

		public ServerCLIExecutableCommand(ServerCLICommandInfo serverCLICommandInfo, ServerCLICommandRequest serverCLICommandRequest)
		{
			this.serverCLICommandInfo = serverCLICommandInfo;
			this.serverCLICommandRequest = serverCLICommandRequest;
		}
	}
}
