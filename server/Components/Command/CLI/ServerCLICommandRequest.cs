﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace server
{
	internal struct ServerCLICommandRequest
	{
		public readonly string[] args;
		public readonly string method;

		public ServerCLICommandRequest(string method, string[] args) : this()
		{
			this.method = ServerCLICommandInfo.ToLowerCase(method);
			this.args = args;
		}

		public static bool TryParse(string input, out ServerCLICommandRequest output) 
		{
			if (string.IsNullOrEmpty(input)) {
				output = new ServerCLICommandRequest();
				return false;
			}

			string[] splitted = Regex.Split(input, @"\W+", RegexOptions.Compiled)
									.Where(s => !string.IsNullOrEmpty(s)).ToArray();

			if (splitted.Length == 0) {
				output = new ServerCLICommandRequest();
				return false;
			}

			string[] args = new string[splitted.Length - 1];
			Array.Copy(splitted, 1, args, 0, splitted.Length - 1);
			output = new ServerCLICommandRequest(splitted[0], args);

			return true;
		}

		public override string ToString()
		{
			int i = 0;
			return string.Format(
				"{0} ({1})", 
				method,
				string.Join(", ", args.Select(s => string.Format("arg{0}", i++)))
			);
		}
	}
}
