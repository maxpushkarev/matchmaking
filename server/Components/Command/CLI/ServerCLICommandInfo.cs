﻿using System.Linq;
using System.Reflection;

namespace server
{
	internal sealed class ServerCLICommandInfo
	{
		public readonly MethodInfo method;
		public readonly ServerCLICommandAttribute serverCLICommandAttribute;
		public readonly string key;
		private int paramsCount;

		public ServerCLICommandInfo(MethodInfo method)
		{
			this.method = method;
			this.paramsCount = method.GetParameters().Length;
			this.key = GetKey(method.Name, paramsCount);
			this.serverCLICommandAttribute = method.GetCustomAttribute<ServerCLICommandAttribute>();
		}

		public override string ToString()
		{
			return string.Format(
				"{0} ({1})",
				ToLowerCase(method.Name), string.Join(", ", method.GetParameters().Select((pi) => string.Format("string {0}", pi.Name)))
			);
		}

		public override int GetHashCode()
		{
			return key.GetHashCode();
		}

		private static string GetKey(string methodName, int paramsCount) {
			return string.Concat(ToLowerCase(methodName), paramsCount);
		}

		public static string GetKey(ServerCLICommandRequest entry)
		{
			return GetKey(entry.method, entry.args.Length);
		}

		public static string ToLowerCase(string input)
		{
			return input.ToLowerInvariant();
		}
	}
}