﻿using server.core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace server
{
	internal abstract class ServerCommandConsumerCLI : ServerCommandConsumer
	{
		private Dictionary<string, ServerCLICommandInfo> cliCommands;

		public Dictionary<string, ServerCLICommandInfo> CLICommands
		{
			get { return cliCommands; }
		}

		protected ServerCommandConsumerCLI(string name) : base(name)
		{
			InitCLICommands();
		}

		private void InitCLICommands()
		{
			IEnumerable<ServerCLICommandInfo> cliCommandsParsedCollection =
				GetType()
				.GetMethods(bindings)
				.Where(mi =>
				{
					if (mi.GetCustomAttribute<ServerCLICommandAttribute>() == null)
					{
						return false;
					}

					ValidateMethod(mi);

					return true;
				})
				.Select(mi => new ServerCLICommandInfo(mi));

			if (cliCommandsParsedCollection.GroupBy(d => d.key).Any(g => g.Count() > 1))
			{
				throw new ServerCLICommandException(
					this, string.Format("There are some collisions within CLI command methods!")
				);
			}

			cliCommands = cliCommandsParsedCollection.ToDictionary(d => d.key);
		}

		private void ValidateMethod(MethodInfo mi)
		{
			Type returnType = mi.ReturnParameter.ParameterType;

			if (!returnType.Equals(typeof(void)))
			{
				throw new ServerCLICommandException(
					this, string.Format("Method = {0} ; Return param must be void type (actual type = {1})", mi.Name, returnType.Name)
				);
			}

			Array.ForEach<ParameterInfo>(mi.GetParameters(), (pi) => {

				if (pi.ParameterType.Equals(typeof(string)))
				{
					return;
				}

				throw new ServerCLICommandException(
					this, string.Format("Method = {0} ; Argument '{1}' must be string type (actual type = {2})", mi.Name, pi.Name, pi.ParameterType.Name)
				);

			});
		}

		public ServerCLICommandInfo GetCLICommand(ServerCLICommandRequest request)
		{
			string key = ServerCLICommandInfo.GetKey(request);
			if (cliCommands.ContainsKey(key))
			{
				return cliCommands[key];
			}
			return null;
		}

	}
}
