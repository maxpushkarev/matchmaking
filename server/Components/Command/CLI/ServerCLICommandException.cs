﻿using server.core;

namespace server
{
	internal sealed class ServerCLICommandException : ServerCommandConsumerException
	{
		public ServerCLICommandException(ServerCommandConsumer consumer, string msg) :
			base(consumer, msg)
		{
		}
	}
}
