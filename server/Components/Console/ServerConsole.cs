﻿using Ninject;
using server.core;
using System;

namespace server
{
	internal sealed class ServerConsole : ServerComponent
	{
		[Inject]
		public ServerCommandConsumerCLI ServerCommandConsumerCLI { get; set; }

		private const string OUTPUT_FORMAT = ">>> {0}";
		private const string SEPARATOR_STRING = "--------------------------------";

		public ServerConsole() : base("ServerConsole")
		{
		}

		public override void Start()
		{
			Console.WriteLine("Waiting for server commands...");
			Console.WriteLine(@"Run ""Help"" to discover commands...");
			base.Start();
		}

		protected override void Run()
		{
			string cmd = Console.ReadLine();

			ServerCLICommandRequest request;
			if (!ServerCLICommandRequest.TryParse(cmd, out request))
			{
				Console.WriteLine(OUTPUT_FORMAT, string.Format("invalid command: [{0}]", cmd));
				return;
			}

			Console.WriteLine(OUTPUT_FORMAT, ApplyCLICommandRequest(request));
			Console.WriteLine(SEPARATOR_STRING);
		}

		private string ApplyCLICommandRequest(ServerCLICommandRequest request) {
			ServerCLICommandInfo serverCLICommandInfo = ServerCommandConsumerCLI.GetCLICommand(request);
			if (serverCLICommandInfo == null)
			{
				return string.Format("unknown command: [{0}]", request.ToString());
			}
			else
			{
				ServerCommandConsumerCLI.PutCommand(new ServerCLIExecutableCommand(serverCLICommandInfo, request));
				return string.Format("execution of command: [{0}]", serverCLICommandInfo);
			}
		}
	}
}
