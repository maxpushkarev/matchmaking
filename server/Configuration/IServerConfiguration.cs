﻿using System;

namespace server
{
	internal interface IServerConfiguration
	{
		int LobbiesCount { get; }
		Type LobbyType { get; }
		Type MatchmakingType { get; }
		object[] LobbyArguments { get; }
	}
}
