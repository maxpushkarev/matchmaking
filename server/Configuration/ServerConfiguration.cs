﻿using commons;
using lobby;
using matchmaking;
using System;

namespace server
{
	internal class ServerConfiguration : BaseConfiguration, IServerConfiguration
	{
		private int lobbiesCount;

		public ServerConfiguration()
		{
			lobbiesCount = ServerSettings.Default.LobbiesCount;
		}

		public int LobbiesCount 
		{
			get { return lobbiesCount; }
		}

		public Type LobbyType
		{
			get { return typeof(LobbyServerComponent); }
		}

		public Type MatchmakingType
		{
			get { return typeof(MatchmakingServerComponent); }
		}

		public object[] LobbyArguments
		{
			get { return new object[] { }; }
		}
	}
}
