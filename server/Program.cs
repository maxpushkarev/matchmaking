﻿using commons;
using server.core;
using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("server.test")]
namespace server
{
	internal class Program
	{
		public static void Main(string[] args)
		{
			try
			{
				CommonInjectorActivator.Bind<ICommonConfiguration>(ConfigurationFactory<CommonConfiguration>.Get());
				CommonInjectorActivator.Bind<IServerConfiguration>(ConfigurationFactory<ServerConfiguration>.Get());

				ServerCommandProcessor serverCommandProcessor = new ServerCommandProcessor();
				CommonInjectorActivator.Bind<ServerCommandConsumerCLI>(serverCommandProcessor);
				ServerNetwork serverNetwork = new ServerNetwork();
				CommonInjectorActivator.Bind<ServerNetwork>(serverNetwork);

				CommonInjectorActivator.Get<ServerCommandProcessor>();
				CommonInjectorActivator.Get<ServerNetwork>();

				using (serverCommandProcessor)
				{
					using (serverNetwork)
					using (ServerComponent serverConsole = CommonInjectorActivator.Get<ServerConsole>())
					{
						serverCommandProcessor.Start();
						serverConsole.Start();
						serverNetwork.Start();

						Console.WriteLine("Server started!");
						serverCommandProcessor.Wait();
					}

				}
			}
			finally
			{
				Console.WriteLine("Server stopped!");
			}
		}
	}
}
